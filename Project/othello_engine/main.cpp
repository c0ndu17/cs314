extern "C" {
#include "board.h"
#include "comms.h"
#include "game.h"
}
#include <dlfcn.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <set>
#include <string>
#include <fstream>
#include <stdexcept>
#include <boost/format.hpp>
#include <sstream>
#include <iostream>
#include <time.h>
#include <mpi.h>

#define GAME_HEIGHT 8
#define GAME_WIDTH  8
#define GAME_SIZE   64

void clientPlayer(int myRank, int turn, int serverRank, int rankBegin, int rankEnd, std::vector<Player *> &players);

/** @short A dynamic library loader. **/
class DLL {
	void* handle;
  public:
  	/** Open the given library file. **/
	DLL(std::string const& filename): handle(dlopen(filename.c_str(), RTLD_NOW | RTLD_LOCAL)) {
		if (!handle) throw std::runtime_error(std::string("DLL::DLL(): ") + dlerror());
	}
	/**
	* Get a symbol from library.
	* @param symptr a pointer (can be a function pointer) to store the symbol in.
	* @param name name of the symbol.
	**/
	template<typename T> void sym(T& symptr, std::string const& name) {
		// A hack for conversions because C++ does not allow converting void*
		// (returned by dlsym) into a function pointer. This hack will silently
		// convert ptr into T no matter what type T is (and will b0rk just as
		// nicely if T is something that it shouldn't be).
		union {
			void* ptr;
			T t;
		} sym;
		sym.ptr = dlsym(handle, name.c_str());
		if (!sym.ptr) throw std::runtime_error(std::string("DLL::sym(): ") + dlerror());
		symptr = sym.t;
	}
	/** Close the library automatically when the DLL object ceases to exist. **/
	~DLL() {
		dlclose(handle);
	}
};

class Player {
  public:
	std::string name;
	std::string filename;
	DLL dll;
	int submission;
	size_t loc, size;
	typedef struct Coord (*AIFunc)(int myRank, int rankBegin, int rankEnd, struct Game const* game);
	AIFunc aiPlay;
	int homewins, awaywins, matches, turns, lostMatches, shortMatches, blockedMoves;
	uint64_t cpu;
	Player(std::string const& name, std::string const& filename, int submission, size_t loc, size_t size):
			name(name), filename(filename), dll(filename + ".so"), submission(submission), loc(loc),
			size(size), homewins(0), awaywins(0), matches(0), turns(0), lostMatches(0),
			shortMatches(0), blockedMoves(0), cpu(0) {
		dll.sym(aiPlay, "aiPlay");
	}
	Coord play(int myRank, int rankBegin, int rankEnd, struct Game const* game) {
		clock_t t = clock();
		++turns;
		Coord c = aiPlay(myRank, rankBegin, rankEnd, game);
		cpu += clock() - t;
		return c;
	}
	int score() const { return homewins + awaywins - lostMatches; }
	static bool compareLOC(Player const* p1, Player const* p2) { return p1->loc < p2->loc; }
	static bool compareScore(Player const* p1, Player const* p2) { return p1->score() > p2->score(); }
};

void htmlPrintBoard(struct Game const* game, Coord cursor, std::ostream& html) {
	html << "<pre><span class=board>";
	size_t p1 = 0, p2 = 0;
	html << "<span class=coord>  ";
	for (int x = 0; x < 8; ++x) {
		html << (char)('a' + x) << ' ';
	}
	html << "</span><br>";
	for (int y = 0; y < GAME_HEIGHT; ++y) {
		html << "<span class=coord>" << (char)('1' + y) << "</span>";
		for (int x = 0; x < GAME_WIDTH; ++x) {
			struct Coord pos = coord(x, y);
			bool cursorIsHere = (x == cursor.x && y == cursor.y);
			if (cursorIsHere) html << "<span class=cursor>&gt;</span>";
			else if (x == 0) html << ' ';
			switch (gameSquareType(game, pos)) {
			  case 0: html << "&middot;"; break;
			  case 1:
				++p1;
				html << "<b>1</b>"; break;
			  case 2:
				++p2;
				html << "<i>2</i>"; break;
			}
			if (cursorIsHere) {
				html << "<span class=cursor>&lt;</span>";
			} else if (x + 1 != cursor.x || y != cursor.y) {
				html << ' ';
			}
		}
		html << "<br>";
	}
	html << "</span></pre><p class=status>Score: " << p1 << "&#45;" << p2 << "</p>";
}

/**
* Copy lines from model to html until a line "<+++>" is found. The function
* discards the special line and ends, so that dynamic content may be written
* there, before calling process() again.
**/
void process(std::istream& model, std::ostream& html) {
	for (std::string s; std::getline(model, s) && s != "<+++>"; html << s << '\n');
}

int play(int player1Rank, int player2Rank, Player& player1, Player& player2, std::ostream& mainhtml) {
	struct Game* game = gameConstruct(&player1, &player2, GAME_WIDTH, GAME_HEIGHT);
	static int gamenum = 0;
	std::string filename = (boost::format("participants/%04d.html") % ++gamenum).str();
	std::ifstream model("modelgame.html");
	std::ofstream html(filename.c_str());
	int winner = 0;

	process(model, html);
	html << player1.name << " vs. " << player2.name << "\n";
	process(model, html);
	html << "<h1>" << player1.name << " vs. " << player2.name << "</h1>\n";
	html << "<h2>&nbsp;</h2>";
	htmlPrintBoard(game, coord(-1, -1), html);
	html << std::endl;
    int action = PLAY;
	do {
		int playernum = gameCurrentPlayer(game);
		Player& player = playernum == 1 ? player1 : player2;
		int currentPlayerRank = (playernum == 1) ? player1Rank : player2Rank;

		/* OpenMPI send() and receive() cycle. */
		struct Coord c;
		MPI_Status status;

		MPI_Send(&action, 1, MPI_INT, currentPlayerRank, ACTION, MPI_COMM_WORLD);
		sendGame(game, currentPlayerRank);
		clock_t t = clock();
		MPI_Recv(&c, 2, MPI_INT, currentPlayerRank, AI_PLAY, MPI_COMM_WORLD, &status);
		player.cpu += clock() - t;
		player.turns++;

		winner = gamePlay(game, &player, c);
		bool blocker = gameCurrentPlayer(game) == playernum;
		if (blocker) ++player.blockedMoves;
		html << "<h2>" << player.name;
		if (winner < 0) {
			html << " is disqualified after attempting an invalid move at ";
		} else if (winner == 0) {
			html << (blocker ? " blocks the opponent by taking " : " plays ");
		} else {
			html << " ends the game by taking ";
		}
		html << (char)('a' + c.x) << (char)('1' + c.y) << "</h2>";
		htmlPrintBoard(game, c, html);
		html << std::endl;
		if (winner < 0) throw std::runtime_error(player.name + " disqualified");
	} while (!winner);

	size_t markers[3] = { 0 };
	for (size_t i = 0; i < GAME_SIZE; ++i)
		++markers[gameSquareType(game, coord(i % GAME_WIDTH, i / GAME_HEIGHT))];

	gameDestruct(game);
	process(model, html);

	mainhtml << "<a href=\"" << filename << "\" onclick=\"return game(this)\" class=";

	if (markers[0]) { ++player1.shortMatches; ++player2.shortMatches; }
	if (markers[1] == markers[2]) { mainhtml << "draw"; winner = 0; }
	else if (markers[1] > markers[2]) mainhtml << "home";
	else mainhtml << "away";

	mainhtml << ">" << markers[1] << "&#45;" << markers[2] << "</a>" << std::flush;

	return winner;
}

int main(int argc, char** argv) {
	int myRank, nProcs, serverRank, player1BeginRank, player2BeginRank;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Comm_size(MPI_COMM_WORLD, &nProcs);

	if (nProcs % 2 == 0)
	{
		std::cout << "Error: Number of process must be uneven." << std::endl;

		MPI_Finalize();

		return 1;
	}

	serverRank = 0;
	player1BeginRank = 1;
	player2BeginRank = (nProcs - 1)/2 + 1;

	std::vector<Player*> players;

	try {
		for (int i = 1; i < argc; ++i) {
			std::string s = argv[i];
			size_t spos = s.find(" submission");
			std::string name;
			int submission;

			if (spos == std::string::npos) {
				name = s.substr(0, s.size() - 3);
				submission = -1;
			} else {
				name = s.substr(0, spos);
				std::istringstream iss(s.substr(spos + 12));
				iss >> submission;
			}

			size_t loc = 0, size = 0;
			std::string filebase = s.substr(0, s.size() - 3);
			std::string filename = filebase + ".c";
			{ std::ifstream f(filename.c_str()); for (char ch; f.get(ch);) { ++size; if (ch == '\n') ++loc; } }

			players.push_back(new Player(name, filebase, submission, loc, size));
		}

		// List of players, sorted by LOC
		std::sort(players.begin(), players.end(), Player::compareLOC);

	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
		
		// Clean-up
		for (std::vector<Player*>::iterator it = players.begin(); it != players.end(); ++it) delete *it;

		MPI_Finalize();

		return 2;
	}

	if (myRank == serverRank)
	{
		try {
			std::ifstream model("model.html");
			std::ofstream html("index.html");

			process(model, html);

			html << "\n<table>\n<tr><th>Player</th><th>Lines of Code</th>" \
				"<th>File Size</th><th>Algorithm</th><th>Randomness</th></tr>\n";
			for (size_t i = 0; i < players.size(); ++i) {
			    Player* p = players[i];
			    html << "<tr><td><a href=\"" << p->filename + ".c\">" << p->name << "</a></td><td>";
			    html << p->loc << "</td><td>" << p->size << "</td><td>N/A</td><td>";
			    {
				    std::ifstream f((p->filename + ".so").c_str());
				    std::string tmp;
				    for (char ch; f.get(ch); tmp += ch);
				    html << ((tmp.find("rand") != std::string::npos) ? "Uses rand()" : "No randomness");
			    }
			    html << "</td></tr>\n";
			}

			html << "</table>\n";
			process(model, html);

			// Let the games begin!
			html << "\n<table>\n<tr><th>&nbsp;</th>";
			for (size_t i = 0; i < players.size(); ++i) {
			    html << "<th>" << players[i]->name << "</th>";
			}
			html << "<th class=1>Home Victories</th></tr>\n";

            int action;
			int homewins = 0, awaywins = 0;
			for (size_t i = 0; i < players.size(); ++i) {
			    Player* p1 = players[i];
			    html << "<tr><th>" << p1->name << "</th>";
			    for (size_t j = 0; j < players.size(); ++j) {
				Player* p2 = players[j];
				html << "<td>";
				++p1->matches;
				++p2->matches;
                action = INITIALIZE;
				/* OpenMPI initialise all client players. */
				for (int r = myRank + 1; r < nProcs; r++)
				{
					MPI_Send(&action, 1, MPI_INT, r, ACTION, MPI_COMM_WORLD);
				}
				for (int r = myRank + 1; r < nProcs; r++)
				{
					MPI_Send(&i, 1, MPI_UNSIGNED, r, PLAYER_INDEX, MPI_COMM_WORLD);
					MPI_Send(&j, 1, MPI_UNSIGNED, r, PLAYER_INDEX, MPI_COMM_WORLD);
				}

				int winner = play(player1BeginRank, player2BeginRank, *p1, *p2, html);

				if (winner == 1) { ++p1->homewins; ++p2->lostMatches; }
				if (winner == 2) { ++p2->awaywins; ++p1->lostMatches; }
				html << "</td>";
			    }
			    html << "<td>" << p1->homewins << "</td></tr>" << std::endl;
			    homewins += p1->homewins;
			}
            action = TERMINATE;
			/* OpenMPI send termination to all client players. */
			for (int r = myRank + 1; r < nProcs; r++)
			{
				MPI_Send(&action, 1, MPI_INT, r, ACTION, MPI_COMM_WORLD);
			}

			html << "<tr style=\"vertical-align: bottom;\"><th class=O>Away Victories:</th>";
			for (size_t i = 0; i < players.size(); ++i) {
			    Player* p = players[i];
			    html << "<td>" << p->awaywins << "</td>";
			    awaywins += p->awaywins;
			}

			html << "<td>" << homewins << "<br>" << awaywins << "</td></tr>\n</table>" << std::endl;
			process(model, html);

			// Final results, sorted by score
			std::stable_sort(players.begin(), players.end(), Player::compareScore);
			html << "\n<table>\n<tr><th>Place</th><th>Player</th><th style=\"text-align: right;\">" \
			    "Avg. CPU Time Per Turn</th><th>Opponent Moves Blocked</th><th>Short Matches</th>" \
			    "<th>Matches<br>[Won / Lost]</th><th>Score</th></tr>\n";

			for (size_t i = 0; i < players.size(); ++i) {
			    Player* p = players[i];
			    html << "<tr><td>" << i + 1 << ".</td><td>" << p->name \
				 << "</td><td style=\"text-align: right;\">" << p->cpu / p->turns << \
				 "&nbsp;&mu;s</td><td>" << p->blockedMoves << "</td><td>" << p->shortMatches \
				 << "</td><td>" << p->homewins + p->awaywins << " / " << p->lostMatches \
				 << "</td><td>" << p->score() << "</td></tr>\n";
			}

			html << "</table>\n";
			process(model, html);

		} catch (std::exception& e) {
			std::cerr << e.what() << std::endl;
		}
	}
	else if ((myRank >= player1BeginRank) && (myRank < player2BeginRank)) // player 1
	{
		// Uses OpenMPI
		clientPlayer(myRank, 1, serverRank, player1BeginRank, player2BeginRank, players);
	}
	else // player 2
	{
		// Uses OpenMPI
		clientPlayer(myRank, 2, serverRank, player2BeginRank, nProcs, players);
	}

	// Clean-up
	for (std::vector<Player*>::iterator it = players.begin(); it != players.end(); ++it) delete *it;

	MPI_Finalize();

	return 0;
}

/* clientPlayer: performs the client OpenMPI send-receive cycle. A client player MUST receive an initialise message
 * before any others. */
void clientPlayer(int myRank, int turn, int serverRank, int rankBegin, int rankEnd, std::vector<Player *> &players)
{
	MPI_Status status;
	int data = 1000; // only used for call to MPI_Recv when determining msg type
	struct Game* game = NULL;
	Player *thisPlayer = NULL; // function will crash unless first message is initialise.
	
	MPI_Recv(&data, 1, MPI_INT, serverRank, ACTION, MPI_COMM_WORLD, &status);
	while (data != TERMINATE)
	{
		if (data == INITIALIZE)
		{
			int player1Index, player2Index;
			Player *player1, *player2;

			MPI_Recv(&player1Index, 1, MPI_INT, serverRank, PLAYER_INDEX, MPI_COMM_WORLD,
				 &status);
			MPI_Recv(&player2Index, 1, MPI_INT, serverRank, PLAYER_INDEX, MPI_COMM_WORLD,
				 &status);

			if (game != NULL)
			{
				gameDestruct(game);
			}

			player1 = players[player1Index];
			player2 = players[player2Index];
			thisPlayer = (turn == 1) ? player1 : player2;
			game = gameConstruct(player1, player2, GAME_WIDTH, GAME_HEIGHT);
			game->turn = turn;
		}
		else if (myRank == rankBegin)
		{
			struct Coord coord;
            
			receiveGame(game, serverRank);
			for (int i = myRank + 1; i < rankEnd; i++)
			{
                
				MPI_Send(&data, 1, MPI_INT, i, ACTION, MPI_COMM_WORLD);
				sendGame(game, i);
			}

			coord = thisPlayer->play(myRank, rankBegin, rankEnd, game);
			MPI_Send(&coord, 2, MPI_INT, serverRank, AI_PLAY, MPI_COMM_WORLD);
		}
		else
		{
			receiveGame(game, rankBegin);
			(void) thisPlayer->play(myRank, rankBegin, rankEnd, game);
		}

		MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, ACTION, MPI_COMM_WORLD, &status);
	}
}
