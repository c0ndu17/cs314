#ifndef TKK_AS_C_COMMS_H
#define TKK_AS_C_COMMS_H

#include "game.h"

#define GAME_SIZE 64

enum MsgTypes { INITIALIZE = 0, PLAYER_INDEX, AI_PLAY, TERMINATE, ACTION, PLAY};

/* sendGame: game must be initialised before passing and dest refers to a
 * processes rank. */
void sendGame(struct Game const *game, int dest);

/* receiveGame: game must be initialised before passing and dest refers to a
 * processes rank. Important: the contents of game will have changed on
 * return. */
void receiveGame(struct Game *game, int source);

#endif
