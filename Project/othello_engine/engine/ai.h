#ifndef TKK_AS_C_AI_H
#define TKK_AS_C_AI_H

#include "game.h"

/** 
 * Play as player, using artificial intelligence. Returns the player's move.
 * aiPlay is executed by all the processes in the range [rankBegin, rankEnd).
 *
 *    myRank    - rank of process currently executing this instance.
 *    rankBegin - begin of process range rank.
 *    rankEnd   - end of process range rank (not included).
 **/
struct Coord aiPlay(int myRank, int rankBegin, int rankEnd, struct Game const* game);

#endif
