/*#include "../mpi.h"*/
#include <mpi.h>
#include "board.h"
#include "comms.h"

#define BUFFER_SIZE ((sizeof(int) + sizeof(struct Board) - sizeof(int *)) + sizeof(int)*GAME_SIZE)

void sendGame(struct Game const *game, int dest)
{
	int position;
	char buffer[BUFFER_SIZE];

	position = 0;
	MPI_Pack((void *) &(game->turn), 1, MPI_INT, buffer, BUFFER_SIZE, &position, MPI_COMM_WORLD);
	MPI_Pack((void *) &(game->board->last.x), 1, MPI_INT, buffer, BUFFER_SIZE, &position, MPI_COMM_WORLD);
	MPI_Pack((void *) &(game->board->last.y), 1, MPI_INT, buffer, BUFFER_SIZE, &position, MPI_COMM_WORLD);
	MPI_Pack((void *) &(game->board->width), 1, MPI_UNSIGNED, buffer, BUFFER_SIZE, &position, MPI_COMM_WORLD);
	MPI_Pack((void *) &(game->board->height), 1, MPI_UNSIGNED, buffer, BUFFER_SIZE, &position, MPI_COMM_WORLD);
	MPI_Pack((void *) game->board->array, GAME_SIZE, MPI_INT, buffer, BUFFER_SIZE, &position, MPI_COMM_WORLD);

	MPI_Send(buffer, position, MPI_PACKED, dest, AI_PLAY, MPI_COMM_WORLD);
}

void receiveGame(struct Game *game, int source)
{
	MPI_Status status;
	int position;
	char buffer[BUFFER_SIZE];

	MPI_Recv(buffer, BUFFER_SIZE, MPI_PACKED, source, AI_PLAY, MPI_COMM_WORLD, &status);

	position = 0;
	MPI_Unpack(buffer, BUFFER_SIZE, &position, &(game->turn), 1, MPI_INT, MPI_COMM_WORLD);
	MPI_Unpack(buffer, BUFFER_SIZE, &position, &(game->board->last.x), 1, MPI_INT, MPI_COMM_WORLD);
	MPI_Unpack(buffer, BUFFER_SIZE, &position, &(game->board->last.y), 1, MPI_INT, MPI_COMM_WORLD);
	MPI_Unpack(buffer, BUFFER_SIZE, &position, &(game->board->width), 1, MPI_UNSIGNED, MPI_COMM_WORLD);
	MPI_Unpack(buffer, BUFFER_SIZE, &position, &(game->board->height), 1, MPI_UNSIGNED, MPI_COMM_WORLD);
	MPI_Unpack(buffer, BUFFER_SIZE, &position, game->board->array, GAME_SIZE, MPI_INT, MPI_COMM_WORLD);
}
