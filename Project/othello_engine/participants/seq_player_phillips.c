#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <unistd.h>

#include "../engine/ai.h"
#include "../engine/util.h"
#include "../engine/comms.h"
#include "../engine/board.h"

struct List_t {
    struct Game* game;
    struct Coord local_coord;
    int local_num_flips;
    struct List_t* next;
};


struct Coord aiPlay(int myRank, int rankBegin, int rankEnd, struct Game const* game);
int NegaMax(struct Game const* game, struct Coord pos, int depth, int alpha, int beta, int colour, int numFlips);
struct List_t* gen_child(struct Game const* game);
int GameOver(struct Board *b);
void printBoard(struct Board *b);
int player_num;

int sign[2]={1,-1};   //0 is blue, 1 is red
int max_depth = 8;

struct Coord aiPlay(int myRank, int rankBegin, int rankEnd, struct Game const* game)
{
    struct Coord pos = { 0, 0 };
    struct Coord tmp_coord = {0, 0};

    if (myRank == rankBegin)
    {
//        printf("\nAlllive.....\n");
        int x, y, width, height, numFlips, tmp, max=INT_MIN;
 //       printBoard(game->board);
        player_num = gameCurrentPlayer(game);
        for (y = 0, height = gameHeight(game); y < height; y++)
        {
            for (x = 0, width = gameWidth(game); x < width; x++)
            {
                tmp_coord = coord(x, y);
                if ((numFlips = gameNumFlips(game, tmp_coord))>0){
 //                   printf("\nDAFUQ i, j\n");
   //                 printf("FOOOOORRR\t%d\t%d\n", y, x);
                    tmp = NegaMax(game, tmp_coord, 0, -1*INT_MAX, INT_MAX, gameCurrentPlayer(game), gameNumFlips(game, tmp_coord));
     //               printf("NEEEEEGGGGAAAA returned...\t%d\n", tmp);
                    if( tmp > max){
                        max = tmp;
                        pos = tmp_coord;
                    }
                }
            }
        }
 //       printf("\n--------------------------------\nmax:\t%d x:\t%d y:\t%d\t CurrentPlayer:\t%d\n", max, pos.x, pos.y, gameCurrentPlayer(game));
        if ((numFlips = gameNumFlips(game, pos))>0)
        {
            sendGame(game, rankBegin + 1);
            return pos;
        }
    }
    else
    {
        struct Game *localGame = gameCopy(game);
        receiveGame(localGame, rankBegin);
        gameDestruct(localGame);
    }
    return pos;
}

int NegaMax(struct Game const* game, struct Coord pos, int depth, int alpha, int beta, int colour, int numFlips)
{
    int best_val, tmp, count = 0;

    struct List_t *Children, *tmp_node;
    if (GameOver(game->board)==1|| depth == max_depth){
 //       printf("\nGameOver== %d\n", GameOver(game->board));
  //      printf("numFlips:\t%d\n", numFlips);
        if (colour == gameCurrentPlayer(game))
            return numFlips;
        else return -1*numFlips;
    }
    best_val = -1*INT_MAX;

    Children = gen_child(game);
    while(Children!=NULL){
        if (colour == 1) tmp = -1*NegaMax(Children->game, Children->local_coord, depth+1, -1*beta, -1*alpha, 2, Children->local_num_flips);
        else if(colour == 2) tmp = -1*NegaMax(Children->game, Children->local_coord, depth+1, -1*beta, -1*alpha, 1, Children->local_num_flips);
        if ( tmp > best_val) {
 //           printf("\ntmp val= %d x: %d y: %d\n", tmp, Children->local_coord.x, Children->local_coord.y);
            best_val = tmp;
        }
        if (tmp > alpha) {
            alpha = tmp;
        }
        if (alpha >= beta) {
            break;
        }
        if (Children->next == NULL) {
            break;
        }
        tmp_node = Children;
        Children = Children->next;
        gameDestruct(tmp_node->game);
        free(tmp_node);
        count++;
    }

    return best_val;

    /*
    function negamax(node, depth, α, β, color)
        if depth = 0 or node is a terminal node
            return color * the heuristic value of node
        bestValue := -∞
        childNodes := GenerateMoves(node)
        childNodes := OrderMoves(childNodes)
        foreach child in childNodes
            val := -negamax(child, depth - 1, -β, -α, -color)
            bestValue := max( bestValue, val )
            α := max( α, val );
            if α ≥ β
                break
        return bestValue

            Initial call for Player A's root node
rootNodeValue := negamax( rootNode, depth, -∞, +∞, 1)

            Initial call for Player B's root node
rootNodeValue := -negamax( rootNode, depth, -∞, +∞, -1)

    if ((GameOver(b) || depth>MaxDepth))
        return sign[color]*Analysis(b);
    int max = -infinity;
    for (each legal move m in board b) {
        copy b to c;
        make move m in board c;
        int x= - NegaMax(c, depth+1, 1-color, -beta, -alpha);
        if (x>max) max = x;
        if (x>alpha) alpha = x;
        if (alpha>=beta) return alpha;
    }
    return max;
    */
}

struct List_t* gen_child(struct Game const* game)
{

    struct Coord pos = { 0, 0 };
    struct Coord tmp_coord = {0, 0};
    int x, y, width, height, tmp;
    struct List_t *board_children=malloc(sizeof(struct List_t)), *tmp_list;
    board_children->next=NULL;
 //   printBoard(game->board);
    for (y = 0, height = gameHeight(game); y < height; y++)
    {
        for (x = 0, width = gameWidth(game); x < width; x++)
        {
            tmp_coord = coord(x, y);
            if ((tmp = gameNumFlips(game, tmp_coord))>0)
            {
                board_children->game = gameCopy(game);
                board_children->local_coord = tmp_coord;
                board_children->local_num_flips = gameNumFlips(board_children->game, board_children->local_coord);
                gamePlayCurrent(board_children->game, tmp_coord);
                tmp_list = malloc(sizeof(struct List_t));
                tmp_list->next = board_children;
                board_children= tmp_list;
            }
        }
    }
    board_children=board_children->next;
    return board_children;
}

int GameOver (struct Board *b) {
    int i, j, count_tiles=0, count_white=0, count_black=0, width, height;
    width = b->width;
    height= b->height;
    for( i = 0; i< height; i++){
        for(j = 0; j < width; j++){
            if (b->array[i*width+j] == 1) {
                count_white++;
                count_tiles++;
            }
            else if (b->array[i*width+j] == 2) {
                count_black++;
                count_tiles++;
            }
        }
    }
    if ((count_black==0||count_white==0)||count_tiles==width*height)
        return 1;
    return 0;
}

void printBoard(struct Board *b)
{
    int i, j, width, height;
    width = b->width;
    height = b->height;
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            printf("%d\t", b->array[(i*width)+j]);
        }
        printf("\n");
    }

}
