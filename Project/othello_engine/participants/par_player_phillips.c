/**
 *  @file par_player_phillips.c
 *  @mainpage 02/05 Othello Project
 *
 *  @author G.W.A. Phillips
 *  This program acts a as an api for a simulated othello player. Utilising the negamax,
 *  a simplified negamax, algorithm, in combination with alpha-beta pruning. It
 *  implements the mpi api as well, allowing for intercommunication and a parallel
 *  environment. The Depth is defined with a with #define, the default is 7.
 *
 */
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <unistd.h>

#include "../engine/ai.h"
#include "../engine/util.h"
#include "../engine/comms.h"
#include "../engine/board.h"

struct List_t {
    struct Game* game;
    struct Coord local_coord;
    int local_num_flips;
    struct List_t* next;
};

struct Coord aiPlay(int myRank, int rankBegin, int rankEnd, struct Game const* game);
int NegaHybrid();
int NegaMax(struct Game const* game, struct Coord pos, int depth, int alpha, int beta, int colour, int numFlips);
struct List_t* gen_child(struct Game const* game);
int GameOver(struct Board *b);
void printBoard(struct Board *b);
int player_num;
int eval(struct Game const *game);

int sign[2]={1,-1};     //0 is blue, 1 is red
int max_depth = 8;      //max depth for the minimax search tree.

/***
 * @brief Picks a move, from a given board, to play next.
 *
 * @param myRank The rank of the processor executing the method.
 * @param rankBegin The rank of the first process, in the interval of processes that this process has access to.
 * @param rankEnd The rank of the last process, in the interval of processes that this process has access to.
 * @param game The copy of the gameboard that tile must be placed onto
 *
 * @return the coordinate position at which to place the tile.
 * */

struct Coord aiPlay(int myRank, int rankBegin, int rankEnd, struct Game const* game)
{
    struct Coord pos = { 0, 0 };
    struct Coord tmp_coord = {0, 0};
    int pos_count = 0;
    struct Coord available_pos[64], *loc_list;

    MPI_Datatype postype;
    MPI_Datatype type[2] = { MPI_INT, MPI_INT};
    int blocklen[2] = { 1, 1};
    MPI_Aint displacement[2];
    displacement[0] = &(available_pos[0].x) - (int *)&(available_pos[0]);
    displacement[1] = &(available_pos[0].y) - (int *)&(available_pos[0]);
    MPI_Type_create_struct(2, blocklen, displacement, type, &postype);
    MPI_Type_commit(&postype);


    if (myRank == rankBegin)
    {
        int i, x, y, width, height, numFlips, tmp, max=INT_MIN, disp, old_disp =0;
        struct Coord *thread_list = malloc(sizeof(struct Coord)*(rankEnd - rankBegin));
        player_num = gameCurrentPlayer(game);
        for (y = 0, height = gameHeight(game); y < height; y++)
        {
            for (x = 0, width = gameWidth(game); x < width; x++)
            {
                tmp_coord = coord(x, y);
                if ( (numFlips = gameNumFlips(game, tmp_coord)) > 0 ) {
                    available_pos[pos_count++] = tmp_coord;
                }
            }
        }
        if ((pos_count/(rankEnd - rankBegin)) > 0 ) {
            old_disp = pos_count/(rankEnd - rankBegin);
            for ( i = 1; pos_count > (i+2)*(pos_count/(rankEnd - rankBegin)); i++) {
                disp = pos_count/(rankEnd - rankBegin);
                MPI_Send(available_pos+old_disp, disp, postype, (rankBegin+i), (rankBegin+i)+1, MPI_COMM_WORLD);
                sendGame(game, rankBegin+i);
                old_disp +=disp;
            }
            if ((pos_count/(rankEnd - rankBegin)) != 0) {
                disp = pos_count - ((i)*(pos_count/(rankEnd - rankBegin)));
                MPI_Send(available_pos+old_disp, disp, postype, (rankBegin+i), (rankBegin+i)+1, MPI_COMM_WORLD);
                sendGame(game, rankBegin+i);
                i++;
            }
            while( i < (rankEnd-rankBegin)){
                disp = 0;
                MPI_Send(&disp, 1, MPI_INT, (rankBegin+i), (rankBegin+i)-1, MPI_COMM_WORLD);
                i++;
            }
        } else {
            i=1;
            while( i < (rankEnd-rankBegin)){
                disp = 0;
                MPI_Send(&disp, 1, MPI_INT, (rankBegin+i), (rankBegin+i)-1, MPI_COMM_WORLD);
                i++;
            }
        }
        if (pos_count/(rankEnd - rankBegin)>0) {
            old_disp = pos_count/(rankEnd - rankBegin);
        } else {
            old_disp = pos_count;
        }
        for (i = 0; i < old_disp ; i++) {
            tmp = NegaMax(game, available_pos[i], 0, -1*INT_MAX, INT_MAX, gameCurrentPlayer(game), gameNumFlips(game, available_pos[i]));
            if ( tmp > max) {
                max = tmp;
                pos = available_pos[i];
            }
        }
        for ( i = 1; pos_count > (i+2)*(pos_count/(rankEnd - rankBegin)) && pos_count/(rankEnd - rankBegin) > 0; i++) {
            MPI_Recv(&tmp, 1, MPI_INT, (rankBegin+i), (rankBegin+i), MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv(&tmp_coord, 1, postype, (rankBegin+i), (rankBegin+i), MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            if( tmp > max){
                max = tmp;
                pos = tmp_coord;
            }
        }
        if ((pos_count/(rankEnd - rankBegin)) != 0 && i<(rankEnd- rankBegin)) {
            MPI_Recv(&tmp, 1, MPI_INT, (rankBegin+i), (rankBegin+i), MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv(&tmp_coord, 1, postype, (rankBegin+i), (rankBegin+i), MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            if( tmp > max){
                max = tmp;
                pos = tmp_coord;
            }
        }
    }
    else
    {
        int tmp, max = INT_MIN, k, tag;
        int length, length2;
        struct Coord *pos_list;
        struct Game *localGame = gameCopy(game);
        MPI_Status status;

        MPI_Probe(rankBegin, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        if (status.MPI_TAG==myRank+1) {
            MPI_Get_count(&status, postype, &length);
            pos_list = malloc(sizeof(struct Coord)*length);
            MPI_Recv(pos_list, length, postype, rankBegin, myRank+1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Probe(rankBegin, AI_PLAY, MPI_COMM_WORLD, &status);
            receiveGame(localGame, rankBegin);
        } else {
            MPI_Recv(&length, 1, MPI_INT, rankBegin, status.MPI_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        if ( length !=0 ) {
            for (k = 0; k < length; k++) {
                tmp = NegaMax(localGame, pos_list[k], 0, -1*INT_MAX, INT_MAX, gameCurrentPlayer(game), gameNumFlips(game, pos_list[k]));
                if (tmp > max) {
                    max = tmp;
                    pos = pos_list[k];
                }
            }
            MPI_Send(&max, 1, MPI_INT, rankBegin, myRank, MPI_COMM_WORLD);
            MPI_Send(&pos, 1, postype, rankBegin, myRank, MPI_COMM_WORLD);
        }
        gameDestruct(localGame);
    }
    return pos;
}

/**
 * @brief NegaMax algorithm, with alpha beta pruning. Checking the highest number of flips from the root nodes.
 *
 * @param game The gameboard on which to run the algorithm
 * @param pos Coordinate to evaluate.
 * @param depth Current depth of the algorithm.
 * @param alpha The alpha value at which to prune.
 * @param beta The beta value at which to prune.
 * @param colour colour of the player, 1 or 2 in the case of the othello engine.
 * @param numFlips The number of flips that could be accrued, if the tile where placed at that position, with the given gameboard.
 *
 * @return The heuristic value of the tile at that position.
 **/
int NegaMax(struct Game const* game, struct Coord pos, int depth, int alpha, int beta, int colour, int numFlips)
{
    int best_val, tmp, count = 0;

    struct List_t *Children, *tmp_node;
    if (GameOver(game->board)==1|| depth == max_depth){
        if (colour == gameCurrentPlayer(game))
            return eval(game);
        else return -1*eval(game);
    }
    best_val = -1*INT_MAX;

    Children = gen_child(game);
    while(Children!=NULL){
        if (colour == 1) tmp = -1*NegaMax(Children->game, Children->local_coord, depth+1, -1*beta, -1*alpha, 2, Children->local_num_flips);
        else if(colour == 2) tmp = -1*NegaMax(Children->game, Children->local_coord, depth+1, -1*beta, -1*alpha, 1, Children->local_num_flips);
        if ( tmp > best_val) {
            best_val = tmp;
        }
        if (tmp > alpha) {
            alpha = tmp;
        }
        if (alpha >= beta) {
            break;
        }
        if (Children->next == NULL) {
            break;
        }
        tmp_node = Children;
        Children = Children->next;
        gameDestruct(tmp_node->game);
        free(tmp_node);
        count++;
    }

    return best_val;
}

/***
 * @brief generates child game states from the given parent board, searching for possible tile placements and storing them in a Linked-List structure.
 * @param game a game, of which to generate the children from.
 * @return A list of possible game states from the current board.
 * */
struct List_t* gen_child(struct Game const* game)
{

    struct Coord pos = { 0, 0 };
    struct Coord tmp_coord = {0, 0};
    int x, y, width, height, tmp;
    struct List_t *board_children=malloc(sizeof(struct List_t)), *tmp_list;
    board_children->next=NULL;
    for (y = 0, height = gameHeight(game); y < height; y++)
    {
        for (x = 0, width = gameWidth(game); x < width; x++)
        {
            tmp_coord = coord(x, y);
            if ((tmp = gameNumFlips(game, tmp_coord))>0)
            {
                board_children->game = gameCopy(game);
                board_children->local_coord = tmp_coord;
                board_children->local_num_flips = gameNumFlips(board_children->game, board_children->local_coord);
                gamePlayCurrent(board_children->game, tmp_coord);
                tmp_list = malloc(sizeof(struct List_t));
                tmp_list->next = board_children;
                board_children= tmp_list;
            }
        }
    }
    board_children=board_children->next;
    return board_children;
}

/***
 * @brief Determines if the game has finished either by full board, as well as being in an unplayable position.
 * @param b A game board to evaluate.
 * @return returns 1 if the game has finished, else 0;
 * */
int GameOver (struct Board *b) {
    int i, j, count_tiles=0, count_white=0, count_black=0, width, height;
    width = b->width;
    height= b->height;
    for( i = 0; i< height; i++){
        for(j = 0; j < width; j++){
            if (b->array[i*width+j] == 1) {
                count_white++;
                count_tiles++;
            }
            else if (b->array[i*width+j] == 2) {
                count_black++;
                count_tiles++;
            }
        }
    }
    if ((count_black==0||count_white==0)||count_tiles==width*height)
        return 1;
    return 0;
}

/***
 * @brief prints the given board
 * @param b A board to print.
 * */
void printBoard(struct Board *b)
{
    int i, j, width, height;
    width = b->width;
    height = b->height;
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            printf("%d\t", b->array[(i*width)+j]);
        }
        printf("\n");
    }

}

/***
 * @brief performs the evaluation function.
 *
 * @param game board to count the current number of tiles
 *
 * @return the amount of that players tiles on the board.
 * */
int eval(struct Game const *game){
    int i, j, ret_val=0, color;
    for(i =0; i< game->board->height; i++){
        for(j=0; j< game->board->width; j++){
            if(game->board->array[(i*game->board->width)+j] == gameCurrentPlayer(game))
                ret_val++;
        }
    }
    return (ret_val/(game->board->height*game->board->width))*100;
}
