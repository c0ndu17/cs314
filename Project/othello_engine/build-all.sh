#!/bin/sh

echo 'Compiling the competition program ...'
mpicc -c engine/*.c -std=c99 -pedantic -Wall -Wextra
mpic++ -o main main.cpp -Iengine *.o -ldl -std=c++98 -ansi -pedantic -Wall -Wextra -O2 -g
rm *.o

cd participants

echo 'Compiling the AIs ...'
for FOO in *.c; do
        mpicc -o "`echo $FOO | sed 's/\.c/.so/'`" "$FOO" -I../engine ../engine/*.c -fPIC -shared -std=c99 -O2 -g
done

echo 'Running the competition (this takes a long time) ...'
cd ..
mpirun -np 7 ./main participants/*.so
#LD_LIBRARY_PATH=mpirun -np 5 ../main *.so

echo 'Done.'
