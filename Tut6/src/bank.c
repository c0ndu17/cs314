/* vim settings: se ts=4 tw=72
 * File: bank.c
 *
 *          An account, Account holders, Deposit and Withdrawel transactions
 *
 * Run: . run.sh
 *
 * Input:
 *
 * Output:
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include "parser.h"
#include "syntax.h"

#define MAX_ACCS 100


void execute_transaction(struct transact **current_trans);
static void get_args(int argc, char* argv[], int* thread_count_p, char** fname);
static void usage(char* prog_name);
static void deposit(int acc_num, double amount, double* balance);
static void withdrawel(int acc_num, double amount, double* balance);
static void transfer(int acc1, int acc2, double amount);
static void acc_balance(int acc_num, double* balance);

typedef struct account {
    int acc_num;
    double balance;
    omp_lock_t lock;
} acc_t;

acc_t* all_accounts;

/*------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
    int thread_count, i;
    char** fname;

    struct transact **inf;
    struct transact *tmp, *loop_tmp;


    inf = (struct transact **) malloc(sizeof(struct transact *) * (argc - 2));
    fname = (char **) malloc(sizeof(char *) * (argc - 2));
    get_args(argc, argv, &thread_count, fname);
    all_accounts = (acc_t*) malloc(MAX_ACCS*sizeof(acc_t));
    if (all_accounts == NULL) {
        fprintf(stderr,"Memory could not be allocated, exiting\n");
        exit(0);
    }
    for(i = 0; i < MAX_ACCS; i++){
        omp_init_lock(&(all_accounts[i].lock));
    }

    printf("Parsing the transaction files of all users...\n");
    for (i = 0; i< thread_count; i++){
        clear_trans();
        parse_transaction_file(fname[i], &(inf[i]));
    }

    printf("\nParsed\n");
    printf("Executing the transactions of all users in parallel\n");
#   pragma omp parallel private(loop_tmp) num_threads(thread_count)
    {
        int my_rank;
        int count = 0;
        my_rank = omp_get_thread_num();
        loop_tmp = inf[my_rank];
        while (loop_tmp != NULL){
            execute_transaction(&(loop_tmp));
        }
    }
    return EXIT_SUCCESS;
}
void execute_transaction (struct transact **current_trans) {
    struct transact *list_pointer = *(current_trans);
    double balance;
    switch(list_pointer->type){
        case 0:
            omp_set_lock(&(all_accounts[list_pointer->src].lock));
            deposit(list_pointer->src, list_pointer->amount, &balance);
            omp_unset_lock(&(all_accounts[list_pointer->src].lock));
            break;
        case 1:
            omp_set_lock(&(all_accounts[list_pointer->src].lock));
            withdrawel(list_pointer->src, list_pointer->amount, &balance);
            omp_unset_lock(&(all_accounts[list_pointer->src].lock));
            break;
        case 2:
            omp_set_lock(&(all_accounts[list_pointer->src].lock));
            omp_set_lock(&(all_accounts[list_pointer->dest].lock));
            transfer(list_pointer->src, list_pointer->dest, list_pointer->amount);
            omp_unset_lock(&(all_accounts[list_pointer->dest].lock));
            omp_unset_lock(&(all_accounts[list_pointer->src].lock));
            break;
        case 3:
            omp_set_lock(&(all_accounts[list_pointer->src].lock));
            acc_balance(list_pointer->src, &balance);
            omp_unset_lock(&(all_accounts[list_pointer->src].lock));
            break;
        default:
            printf("\nOops, something went wrong x.X\n");
    }
    list_pointer = *(current_trans);
    *(current_trans) = list_pointer->next;
}

/*------------------------------------------------------------------
 * @brief  get_args
 *            Get command line args
 * In args:   argc, argv
 * Out args:  thread_count_p, m_p, n_p
 */
static void get_args(int argc, char* argv[], int* thread_count_p, char* fname[])  {

    int thread_count;
    if (argc < 3) usage(argv[0]);
    *thread_count_p = argc-2;
    thread_count = *thread_count_p;
#   pragma omp parallel num_threads(argc-2)
    {
        int my_rank = omp_get_thread_num();
        fname[my_rank] = malloc(sizeof(char)*strlen(argv[my_rank + 2]));
        strcpy(fname[my_rank], argv[my_rank + 2]);
    }
    if (*thread_count_p <= 0) usage(argv[0]);

}  /* get_args */

/*------------------------------------------------------------------
 * @brief  usage
 *            print a message showing what the command line should
 *            be, and terminate
 * In arg :   prog_name
 */
static void usage (char* prog_name) {
   fprintf(stderr, "usage: %s <thread_count> <datafile.txt> [datafile.txt]\n", prog_name);
   exit(0);
}  /* usage */
/*--------------------------------------------------------------------
 * @brief deposit
 *        Add amount to balance
 * @param acc_num: Account number
 * @param amount:  Amount to deposit
 * @param balance: Balance of acc_num
 */
static void deposit(int acc_num, double amount, double* balance)
{
    all_accounts[acc_num].balance += amount;
    *balance = all_accounts[acc_num].balance;
    printf("\n%d deposited an of amount %f. Balance: %f\n", acc_num, amount, all_accounts[acc_num].balance);
}

/*-------------------------------------------------------------------
 * @brief withdrawel
 *        If amount available, subtract amount from balance
 * @param acc_num: Account Number
 * @param amount:  Amount to withdraw
 * @param balance: Balance of acc_num
 */
static void withdrawel(int acc_num, double amount, double* balance)
{
    if (amount <= all_accounts[acc_num].balance) {
        all_accounts[acc_num].balance -= amount;
        printf("\n%d withdrew an of amount %f. Balance %f\n", acc_num, amount, all_accounts[acc_num].balance);
    }
}

/*--------------------------------------------------------------------
 * @brief transfer
 *           If amount available in acc1,
 *           subtract amount from acc1 balance and add to acc2 balance
 * @param acc1:    Number of account from which money is transferred
 * @param acc2:    Number of account to which money is transferred
 * @param amount:  Amount to transfer
 * @param balance: Balance of acc1
 */
static void transfer(int acc1, int acc2, double amount)
{
    if (amount <= all_accounts[acc1].balance) {
        all_accounts[acc1].balance -= amount;
        all_accounts[acc2].balance += amount;
        printf("\n%d transferred an of amount %f to User %d. Balance for: %d\t%f\t%d\t%f\n", acc1, amount, acc2, acc1, all_accounts[acc1].balance, acc2, all_accounts[acc2].balance);
    }
}

/*--------------------------------------------------------------------
 * @brief balance
 *           Return the current balance of account acc_num
 * @param acc_num: The number of the account
 * @param balance: The current balance of account acc_num
 */
static void acc_balance(int acc_num, double* balance)
{
    *balance = all_accounts[acc_num].balance;
    printf("\n%d Balance: %f\n", acc_num, *balance);
}


/*--------------------------------------------------------------------
 * @brief transactions_user1
 *           List of transactions for user1
 *           User1's account number is 0.
 *
 */
void transactions_user1()
{
    double balance;

    deposit(0, 100, &balance);
    deposit(0, 200, &balance);
    deposit(0, 300, &balance);
    withdrawel(0, 200, &balance);
    withdrawel(0, 100, &balance);
    withdrawel(0, 50, &balance);
    transfer(1, 0, 200);
    acc_balance(0, &balance);
    printf("Account 0 balance after completion of transaction batch: %f\n", balance);
}

/*--------------------------------------------------------------------
 * @brief transactions_user2
 *           List of transactions for user2
 *           User2's account number is 0.
 *
 */
void transactions_user2()
{
    double balance;

    deposit(0, 400, &balance);
    withdrawel(0, 200, &balance);
    deposit(0, 100, &balance);
    withdrawel(0, 50, &balance);
    withdrawel(0, 150, &balance);
    deposit(0, 300, &balance);
    transfer(0, 1, 300);
    acc_balance(1, &balance);
    printf("Account 1 balance after completion of transaction batch: %f\n", balance);
}

