/***
 *  @file loader.c
 *  @author G.W.A. Phillips
 *  @date 23/03/2014
 */
#include "loader.h"
#include "syntax.h"

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>


void load_deposit(int acc_dest, double amount);
void load_withdrawel(int acc_src, double amount);
void load_transfer(int acc_src, int acc_dest, double amount);
void load_balance(int acc_src);
struct transact* get_userlist();
void set_first(struct transact* set_list);


struct transact *first_trans = NULL;
struct transact *current_trans = NULL;
struct transact *trans = NULL;

void set_first(struct transact* set_list){
    first_trans = set_list;
    current_trans=NULL;
    trans=NULL;
}

void clear_trans(){
    first_trans=NULL;
}

/**
 * @brief Loads the deposit into the users transaction list.
 *
 * Initialises, Loads and Inserts in to the deposit list.
 *
 * @param acc_dest integer of the deposit account
 * @param amount amount to add to the balance
 */
void load_deposit(int acc_dest, double amount){
    if(first_trans==NULL){
        first_trans = malloc(sizeof(struct transact));
        current_trans = first_trans;
    } else {
        trans = malloc(sizeof(struct transact));
        current_trans->next = trans;
        current_trans = trans;
    }
    current_trans->type = 0;
    current_trans->amount = amount;
    current_trans->src = acc_dest;
    current_trans->next=NULL;
}

/**
 * @brief Loads the withdrawel transaction into the users transaction list.
 *
 * Initialises, Loads and Inserts the withdrawel into the transaction list.
 *
 * @param acc_src integer of the withdrawel account
 * @param amount amount to subtract from the balance
 */
void load_withdrawel(int acc_src, double amount){
    if(first_trans==NULL){
        first_trans = malloc(sizeof(struct transact));
        current_trans = first_trans;
    } else {
        trans = malloc(sizeof(struct transact));
        current_trans->next = trans;
        current_trans = trans;
    }
    current_trans->type = 1;
    current_trans->amount = amount;
    current_trans->src = acc_src;
}

/**
 * @brief Loads the transfer into the users transaction list.
 *
 * Initialises, Loads and Inserts the transfer of funds into the transaction list.
 *
 * @param acc_src integer of the account to deduct from
 * @param acc_dest integer of the account to deposit into.
 * @param amount amount to transfer between the accounts
 */
void load_transfer(int acc_src, int acc_dest, double amount){
    if(first_trans==NULL){
        first_trans = malloc(sizeof(struct transact));
        current_trans = first_trans;
    } else {
        trans = malloc(sizeof(struct transact));
        current_trans->next = trans;
        current_trans = trans;
    }
    current_trans->type = 2;
    current_trans->amount = amount;
    current_trans->src = acc_src;
    current_trans->dest = acc_dest;
}

/**
 * @brief Loads the balance into the users transaction list.
 *
 * Initialises, Loads and Inserts the balance into the transaction list.
 *
 * @param acc_src integer of the deposit account
 */
void load_balance(int acc_src){
    if(first_trans==NULL){
        first_trans = malloc(sizeof(struct transact));
        current_trans = first_trans;
    } else {
        trans = malloc(sizeof(struct transact));
        current_trans->next = trans;
        current_trans = trans;
    }
    current_trans->type = 3;
    current_trans->src = acc_src;
}

struct transact* get_userlist(){
    return first_trans;
}
