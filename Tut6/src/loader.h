/***
 *  @file loader.h
 *  @author G.W.A. Phillips
 *  @date 23/03/2014
 */
#ifndef _LOADER_H
#define _LOADER_H

struct transact {
    double amount;
    int type;
    int src;
    int dest;
    struct transact *next;
};

void load_deposit(int acc_dest, double amount);
void load_withdrawel(int acc_src, double amount);
void load_transfer(int acc_src, int acc_dest, double amount);
void load_balance(int acc_src);
struct transact* get_userlist();
void set_first(struct transact* set_list);

#endif
