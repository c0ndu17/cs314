/**
 * @file syntax.h
 */
#ifndef _SYNTAX_H
#define _SYNTAX_H

#define DP "deposit"
#define WD "withdrawel"
#define TR "transfer"
#define BL "acc_balance"

#define LEFTBRACKET 40
#define RIGHTBRACKET 41
#define COMMA 44
#define WHITESPACE 32

#define TRUE 1
#define FALSE 0

#endif

