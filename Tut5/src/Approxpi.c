#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <time.h>

#define FALSE 0
#define TRUE 1
double Approxpi (int n);

int main(int argc, char* argv[]){
    int thread_count = strtol(argv[1], NULL, 10);
    long long int n = atoll(argv[2]);
    if(atoll(argv[2])<0){
        printf("Negative amount of trials...\n[Killing Program]\n");
    }
    double start, finish;
       /* execution */
    double global_result =0;
    printf("\n Estimating Pi  using %llu repeats with %d threads\n", n, thread_count);
    start = omp_get_wtime();

#   pragma omp parallel num_threads(thread_count)
    {
        double my_result =0;
        my_result += Approxpi(n);
#   pragma omp critical
        global_result+=my_result;
    }

    finish = omp_get_wtime();
    printf("Pi:\n\t%f\n", global_result/thread_count);
    printf("Elapsed time = %f seconds\n", finish - start);
    return EXIT_SUCCESS;
}

double Approxpi (int n) {

    struct drand48_data randBuffer;
    double x, y, dist;
    int i = 0;
    double pi;
    int hits =0;
    int my_rank = omp_get_thread_num();
    int thread_count = omp_get_num_threads();

    /* Initialise the buffer */
    srand48_r( time(NULL)<< (my_rank*(my_rank+1)), &randBuffer);

    /* Generate a random number and store it
     * in variable x; drand48_r() sets the 2nd
     * param to a number in the range [0.0, 1.0)
     */

    for(i= 1; i< n/thread_count; i++) {
        drand48_r(&randBuffer, &x);
        drand48_r(&randBuffer, &y);
        dist = sqrt(pow(x, 2) + pow(y, 2));
        if (dist <= 1)
            hits = hits + 1;
    }
    if(thread_count > 0 && n > 0){
        pi = (double) 4*hits/(n/thread_count);
        return pi;
    }
    return -1;
}
