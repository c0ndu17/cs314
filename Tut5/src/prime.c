#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>

#define FALSE 0

static int isprime(int n);
static int block_prime(int n);

int main(int argc, char* argv[]){
    int thread_count = strtol(argv[1], NULL, 10);
    int n = atoi(argv[2]);
    double start, finish;
       /* execution */
    int global_result =0;
    start = omp_get_wtime();

#   pragma omp parallel num_threads(thread_count)
    {
        int my_result =0;
        my_result += block_prime(n);
#   pragma omp critical
        global_result+=my_result;
    }
    finish = omp_get_wtime();
    printf("\nNumber of primes below %d = %d\n", n, global_result);
    printf("Elapsed time = %f seconds\n", finish - start);
    return EXIT_SUCCESS;
}

static int block_prime(int n) {
    int my_rank = omp_get_thread_num();
    int thread_count = omp_get_num_threads();
    int count=0;
    int i;
    for (i = my_rank; i < n; i = i + thread_count){
        if ( isprime(i)) count++;

    }

    return count;
}

static int isprime(int n)
{
    int i;
    for(i=2;i<=(int)(sqrt((double) n));i++){
        if (n%i==0) return FALSE;
    }
    return n>1;
}
