/**?
 *  @file manager.c
 *  @author George W.A. Phillips, 16512561
 *  @date 06/02/2014
 *?**/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "manager.h"

#define QUANTUM 1
#define INTBITSIZE 32
#define TRUE 1
#define FALSE 0

void process_request (struct processControlBlock *current, struct instruction *instruct, struct resourceList **resource); //Handles the request resource instruction.
void process_release (struct processControlBlock *current, struct instruction *instruct, struct resourceList **resource); // Handles the release resource instruction
void process_send_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail);                                                                     //DONE
void process_receive_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail);
void set_process_ready (struct cpuSchedule *schedule, int proc); //Sets the bit in the readyQueue for process proc.
void set_process_waiting (struct cpuSchedule *schedule, int proc); //Sets the bit in the waitingQueue for process proc.
void set_process_terminated (struct cpuSchedule *schedule, int proc); //Sets the bit in the terminatedQueue for process proc.
void set_bit (int *queue, int proc); //Sets a bit in the queue.                                                                                                                     //DONE
int read_bit (int *queue, int proc); //Tests a bit in the queue.                                                                                                                    //DONE
void clear_bit (int *queue, int proc); //Resets a bit in the queue.
void proc_wait (struct processControlBlock *p);
int processes_finished (struct processControlBlock *firstPCB); //Iterates over each of the loaded processes and checks if it has been terminated.                                   //DONE
int processes_deadlocked (struct processControlBlock *firstPCB); // Iterates over each of the loaded process and check if all the processes are deadlocked.
void schedule_processes (struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail); //Schedules each instruction of each process in a round-robin fashion. The number of instruction to execute for each process is governed by the QUANTUM variable.

/**
 *  Debuging Definitions
 **?*/
void print_res(struct processControlBlock *p, struct resourceList *res);



/**********************
 *  @brief Handles the request resource instruction
 *
 **********************/

void process_request (struct processControlBlock *current, struct instruction *instruct, struct resourceList **resource) //Handles the request resource instruction.
{
    struct processControlBlock *pcbIter = current, *p;
    struct resourceList *resourceIter = *(resource), *parent, *tmp;//= (struct resourceList *)malloc(sizeof(struct resourceList *));
    struct resourceList *tmp_b;
    current->processState = RUNNING;
    set_process_ready(pcbIter->cpuSchedulePtr, pcbIter->pagePtr->number);
    printf("Acquiring resource %s for %s...\n", instruct->resource, pcbIter->pagePtr->name);
    parent = NULL;
    /**
     * if request and not in list... set to wait
     * else acquire
     * */
    if ( resourceIter != NULL) {
        while ( strcmp(resourceIter->name, instruct->resource) != 0 ) {
            parent = resourceIter;
            resourceIter = resourceIter->next;
            if (resourceIter == NULL) {
                proc_wait(current);
                return;
            }
        }
        if(parent!=NULL){
            if(resourceIter->next != NULL) parent->next = resourceIter->next;
            else parent->next = NULL;
        } else {
            if(resourceIter->next != NULL){
                resourceIter = resourceIter->next;
            } else {
                resourceIter = NULL;
            }
        }
        tmp = resourceIter;
        tmp->next = current->resourceListPtr;
        current->resourceListPtr = tmp;
    } else {
        proc_wait(current);
    }
    if(pcbIter->processState != WAITING){
        current->nextInstruction = current->nextInstruction->next;
        dealloc_instruction(instruct);
        printf("Request Completed for Processs %s!\n", current->pagePtr->name);
    }
    return;
}

/**********************
 *  @brief Handles the release resource instruction
 *
 **********************/

void process_release (struct processControlBlock *current, struct instruction *instruct, struct resourceList **resource)
{
    struct processControlBlock *pcbIter, *p;
    struct resourceList *resourceIter, *res_iter;
    struct resourceList *parent, *tmp;//= (struct resourceList *)malloc(sizeof(struct resourceList));

    current->processState = RUNNING;
    set_process_ready(current->cpuSchedulePtr, current->pagePtr->number);
    resourceIter = current->resourceListPtr;
    pcbIter = current;
    parent = NULL;
    printf("Releasing resource %s from Process %s... ", instruct->resource, pcbIter->pagePtr->name);
    if(resourceIter!=NULL){
        while ( strcmp(resourceIter->name, instruct->resource) != 0 ) {
            parent = resourceIter;
            resourceIter = resourceIter->next;
            if (resourceIter == NULL) {
                printf("Resource %s for release not allocated to Process %d\n", instruct->resource, current->pagePtr->number );
                return;
            }
        }
        if (parent!=NULL) {
            if(resourceIter->next != NULL) parent->next = resourceIter->next;
            else parent->next = NULL;
        } else {
            if(resourceIter->next != NULL){
                current->resourceListPtr = current->resourceListPtr->next;
            } else {
                current->resourceListPtr = NULL;
            }
        }
        tmp = resourceIter;
        tmp->next = *(resource);
        *(resource) = tmp;
    } else {
        printf("Resource %s for release not allocated to Process %s\n", instruct->resource, current->pagePtr->name );
    }
    current->nextInstruction = current->nextInstruction->next;
    dealloc_instruction(instruct);
    printf("\tReleased!\n");
    return;
}



/**
 * @brief Sends the message the prescribed mailbox.
 *
 * Sends the message specified in the instruction of the current process, to
 * the mailbox specified in the instruction. The function gets the pointer to
 * the first mailbox and loops through all the mailboxes to find the one to
 * which the message must be sent.
 *
 * @param pcb The current process which instruct us to send a message.
 * @param instruct The current send instruction which contains the message.
 * @param mail The list of available mailboxes.
 */
void process_send_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail)
{
    struct processControlBlock *pcbIter;
    struct resourceList *resourceIter;
    struct mailbox *mailIter;

    struct mailbox *currentMbox;

    pcb->processState = RUNNING;

    currentMbox = mail;
    do {
        if ( strcmp(currentMbox->name, instruct->resource) == 0 ) {
            /* We found the mailbox in which a message should be left */
            break;
        }
        currentMbox = currentMbox->next;
    } while ( currentMbox != NULL );

    printf("%s sending message to mailbox %s which says \033[22;31m %s \033[0m\n",
            pcb->pagePtr->name, currentMbox->name, instruct->msg);

    currentMbox->msg = instruct->msg;
    pcb->nextInstruction = pcb->nextInstruction->next;
    dealloc_instruction(instruct);
}

/**
 * @brief Retrieves the message from the mailbox specified in the instruction
 * and stores it in the instruction message field.
 *
 * The function loops through the available mailboxes and finds the mailbox
 * from which the message must be retrieved. The retrieved message is stored
 * in the message field of the instruction of the process.
 *
 * @param pcb The current process which requests a message retrieval.
 * @param instruct The instruction to retrieve a message from a specific
 * mailbox.
 * @param mail The list of mailboxes.
 */
void process_receive_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail)
{
    struct processControlBlock *pcbIter;
    struct resourceList *resourceIter;
    struct mailbox *mailIter;

    struct mailbox *currentMbox;

    pcb->processState = RUNNING;

    currentMbox = mail;
    do {
        if ( strcmp(currentMbox->name, instruct->resource) == 0 ) {
            /* We found the mailbox from which a message must be read. */
            break;
        }
        currentMbox = currentMbox->next;
    } while ( currentMbox != NULL );
    if(currentMbox == NULL){
        proc_wait(pcb);
        return;
    }

    printf("%s retrieved message from mailbox %s which says \033[22;32m %s \033[0m\n",
            pcb->pagePtr->name, currentMbox->name, currentMbox->msg);

    instruct->msg = currentMbox->msg;
    currentMbox->msg = NULL;
    pcb->nextInstruction = pcb->nextInstruction->next;
    dealloc_instruction(instruct);
}


/**
 * @brief   Sets the bit in the readyQueue for process proc.
 *
 * Makes use of the set_bit & clear_bit function, to set the bit for this process.
 *
 * @param   schedule The cpuSchedule struct for the process
 * @param   proc The number of the process
 */
void set_process_ready (struct cpuSchedule *schedule, int proc)
{

    set_bit(schedule->readyQueue, proc);
    clear_bit(schedule->waitingQueue, proc);
    clear_bit(schedule->terminatedQueue, proc);
    return;
}

/**
 * @brief Sets the bit in the waitingQueue for process proc.
 *
 * Makes use of the set_bit & clear_bit function, to set the bit for this process.
 *
 * @param schedule The cpuSchedule struct for the process
 * @param proc The number of the process
 */
void set_process_waiting (struct cpuSchedule *schedule, int proc)
{
    set_bit(schedule->waitingQueue, proc);
    clear_bit(schedule->readyQueue, proc);
    clear_bit(schedule->terminatedQueue, proc);
    return;
}

/**
 * @brief Sets the bit in the terminatedQueue for process proc.
 *
 * Makes use of the set_bit & clear_bit function, to set the bit for this process.
 *
 * @param schedule The cpuSchedule struct for the process
 * @param proc The number of the process
 */
void set_process_terminated (struct cpuSchedule *schedule, int proc)
{
    set_bit(schedule->terminatedQueue, proc);
    clear_bit(schedule->waitingQueue, proc);
    clear_bit(schedule->readyQueue, proc);
    return;
}

/**
 * @brief Sets a bit in the queue
 *
 * Uses the bit test and set function defined in asm.h to set a bit in a 32bit
 * integer.
 *
 * @param queue The array of ints in which the bit must be set.
 * @param proc The number of the process.
 */

void set_bit(int *queue, int proc)
{
    bts(queue[proc/INTBITSIZE], proc%INTBITSIZE);
}

/**
 * @brief Tests a bit in the queue
 *
 * Uses the bit test function defined in asm.h to check the status of a bit in
 * a 32 bit integer.
 *
 * @param queue The array of ints in which the bit must be set.
 * @param proc The number of the process.
 *
 * @return The observed status of the bit.
 */
int read_bit(int *queue, int proc)
{
    return bt(queue[proc/INTBITSIZE], proc%INTBITSIZE);
}

/**
 * @brief Resets a bit in the queue
 *
 * Uses the bit test and reset function defined in asm.h to clear the bit
 * specified by the proc in a 32 bit integer.
 *
 * @param queue The array of ints in which the bit must be set.
 * @param proc The number of the process.
 */
void clear_bit(int *queue, int proc)
{
    btr(queue[proc/INTBITSIZE], proc%INTBITSIZE);
}

/**
 * @brief
 *
 *
 *
 */
void proc_wait (struct processControlBlock *p){
    p->processState = WAITING;
    set_process_waiting(p->cpuSchedulePtr, p->pagePtr->number);
    printf("\tFailed!\n");
    if (p->nextInstruction->resource!=NULL) {
        printf("\t%s, set to WAITING\nResource: %s", p->pagePtr->name, p->nextInstruction->resource);
    }
    else {
        printf("\t%s, set to WAITING\nMessage: %s", p->pagePtr->name, p->nextInstruction->msg);
    }

    return;
}

/**
 * @brief
 *
 *
 *
 */
int processes_finished (struct processControlBlock *firstPCB)
{
    struct processControlBlock *pcbIter;
    pcbIter = firstPCB;
    printf("pcbIter state %d\n",pcbIter->processState);
    do {
        if (pcbIter->processState ==2) return -1;
        pcbIter = pcbIter->next;
    } while(pcbIter!=NULL);
    return 1;
}

/**
 * @brief
 *
 *
 */
int processes_deadlocked (struct processControlBlock *firstPCB)
{

    struct processControlBlock *pcbIter;
    struct resourceList *resourceIter;
    struct mailbox *mailIter;
    return 0;
}

/**
 * @brief
 *
 *
 void process_release (struct processControlBlock *current, struct instruction *instruct, struct resourceList *resource); // Handles the release resource instruction
 void process_request (struct processControlBlock *current, struct instruction *instruct, struct resourceList *resource); //Handles the request resource instruction.
 void process_send_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail);
 void process_receive_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail);
 */
void schedule_processes (struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail)
{

    /*
     *Add Check function to kill while
     * */
    struct processControlBlock *pcb_save, *p;
    struct instruction *curr_instruct;
    struct resourceList *current_resource = resource;
    int count = 0;
    pcb_save = pcb;
    // if first come first serve
    /*while ( 1 != processes_finished(pcb) ) { // && !processes_deadlocked(pcb)*/
    printf("\nAt Starting pcb...\n");
    do {
        //printf("Process %s in State: %d\n", pcb->pagePtr->name, pcb->processState);
        while (pcb->processState!=TERMINATED) {
            curr_instruct = pcb->nextInstruction;
            if(curr_instruct == NULL) {
                pcb->processState == TERMINATED;
                set_process_terminated(pcb->cpuSchedulePtr, pcb->pagePtr->number);
                break;
            }
            switch (curr_instruct->type){
                case 0:
                    process_request (pcb, curr_instruct, &(resource));
                    count++;
                    break;//
                case 1:
                    process_release (pcb, curr_instruct, &(resource));
                    count++;
                    break;//
                case 2:
                    process_send_message(pcb, curr_instruct, mail);
                    count++;
                    break;//
                case 3:
                    process_receive_message(pcb, curr_instruct, mail);
                    count++;
                    break;//
                default:
                    printf("\nNo valid instruction type <0-3>, returned type was of int %d\n", curr_instruct->type);
                    break;
            }
            print_res(p, resource);
            print_res(pcb, pcb->resourceListPtr);
            /*
               if(pcb->processState==WAITING)
               break;
             */
        }
        pcb = pcb->next;
        count++;
    } while (pcb!=NULL);
    //pcb = pcb_save;
    //}

    //if roundrobin
    return;
}

void print_res(struct processControlBlock *p, struct resourceList *res){
    struct resourceList *tmp = res;
    if(p!=NULL){
        printf("\n----------------------\n\tResourceList\n----------------------\n");
        while(tmp!=NULL){
            printf("\n%s in Process %s resourceList\n", tmp->name, p->pagePtr->name );
            tmp = tmp->next;
        }
    }
    else {
        printf("\n---------------\nSYSTEM RESOURCES\n----------------\n");
        while(tmp!=NULL){
            printf("\n%s in resourceList\n", tmp->name);
            tmp = tmp->next;
        }

    }

}
