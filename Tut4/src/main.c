/**
 * @file main.c
 * @author Francois de Villiers (Demi 2011-2012)
 * @description The main function file for the process management application.
 *
 * @mainpage Process Simulation
 *
 * @section intro_sec Introduction
 *
 * The project consists of 3 main files parser, loader and manager. Which
 * respectively handles the loading, parsing and management of the processes.
 *
 * @section make_sec Compile
 *
 * $ make
 *
 * @section run_sec Execute
 *
 * $ ./process-management data/process.list
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"
#include "loader.h"
#include "manager.h"


void debug_pcb ( struct processControlBlock *pcb );
void debug_mailboxes ( struct mailbox *mail );
FILE *src;

int main(int argc, char** argv) {
  char* filename;
  struct processControlBlock *pcb;
  struct resourceList *resources;
  struct mailbox *mailboxes;
  int FCFS = 1;
  int quant = 0;

  filename = NULL;

  if ( argc < 2 ) {
      printf("Usage: ./my_executable <filename> [-r] [int]\n\tOptions:\n\t\t-r\t for round robin implementation, else it defaults to first come, first serve\n\t\t[int]:\t an integer defining round robins time quantum\n");
      return EXIT_FAILURE;
  }
  if(argc==3){
      if(strcmp(argv[2], "-r")==0){
          quant = 1;
          FCFS = 0;
      } else {
      printf("Usage: ./my_executable <filename> [-r] [int]\n\tOptions:\n\t\t-r\t for round robin implementation, else it defaults to first come, first serve\n\t\t[int]:\t an integer defining round robins time quantum\n");
      return EXIT_FAILURE;
      }
  } else if(argc==4){
      if(strcmp(argv[2], "-r")==0){
          quant = atoi(argv[3]);
          FCFS = 0;
      } else {
      printf("Usage: ./my_executable <filename> [-r] [int]\n\tOptions:\n\t\t-r\t for round robin implementation, else it defaults to first come, first serve\n\t\t[int]:\t an integer defining round robins time quantum\n");
      return EXIT_FAILURE;
      }
  }
  filename = argv[1];
  if((src =fopen(filename, "r"))==NULL){
    printf("\nfile does not exist\n[Killing]");
    exit(0);
  }
//  if(argv[2] == '1') FCFS = 1;

  parse_process_file(filename);

  pcb = get_loaded_processes();
  resources = get_available_resources();
  mailboxes = get_mailboxes();

#ifdef DEBUG
  debug_pcb(pcb);
#endif

  //TODO: Schedule processes
  schedule_processes (pcb, resources, mailboxes, FCFS, quant);

  dealloc_processes();

  return EXIT_SUCCESS;
}

#ifdef DEBUG
void debug_pcb ( struct processControlBlock *pcb ) {
  struct processControlBlock *debug;
  struct instruction *debugInst;

  debug = pcb;
  do {
    printf("PCB %s\n", debug->pagePtr->name);
    printf("State: %d\n", debug->processState);
    debugInst = debug->nextInstruction;
    do {
      if(debugInst == NULL) {
        break;
      }
      printf("(%d, %s, %s)\n", debugInst->type, debugInst->resource, debugInst->msg);
      debugInst = debugInst->next;
    } while(debugInst != NULL);

    debug = debug->next;
  } while(debug != NULL);

}

void debug_mailboxes ( struct mailbox *mail ) {
  struct mailbox *debug;
  debug = mail;
  do {
    printf("Mailbox %s\n", debug->name);
    debug = debug->next;
  } while ( debug != NULL );
}
#endif

