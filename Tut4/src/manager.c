/**?
 *  @file manager.c
 *  @author George W.A. Phillips, 16512561
 *  @date 06/02/2014
 *?**/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "manager.h"

#define QUANTUM 1
#define INTBITSIZE 32
#define TRUE 1
#define FALSE 0

void process_request (struct processControlBlock *current, struct instruction *instruct, struct resourceList **resource); //Handles the request resource instruction.
void process_release (struct processControlBlock *current, struct instruction *instruct, struct resourceList **resource); // Handles the release resource instruction
void process_send_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail);                                                                     //DONE
void process_receive_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail);
void set_process_ready (struct cpuSchedule *schedule, int proc); //Sets the bit in the readyQueue for process proc.
void set_process_waiting (struct cpuSchedule *schedule, int proc); //Sets the bit in the waitingQueue for process proc.
void set_process_terminated (struct cpuSchedule *schedule, int proc); //Sets the bit in the terminatedQueue for process proc.
void set_bit (int *queue, int proc); //Sets a bit in the queue.                                                                                                                     //DONE
int read_bit (int *queue, int proc); //Tests a bit in the queue.                                                                                                                    //DONE
void clear_bit (int *queue, int proc); //Resets a bit in the queue.
void proc_wait (struct processControlBlock *p);
int processes_finished (struct processControlBlock *firstPCB); //Iterates over each of the loaded processes and checks if it has been terminated.                                   //DONE
int processes_deadlocked (struct processControlBlock *firstPCB); // Iterates over each of the loaded process and check if all the processes are deadlocked.
void schedule_processes (struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail, int FCFS, int quantum); //Schedules each instruction of each process in a round-robin fashion. The number of instruction to execute for each process is governed by the QUANTUM variable.
void res_check(struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail);
void fcfs(struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail);
void round_robin(struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail);

/**
 *  Debuging Definitions
 **?*/
void print_res(struct processControlBlock *p, struct resourceList *res);

int glob_check =-1;
int boole = 0;
int prev_waiting;
int prev_terminator;
int start_ready;
int quantum = 0;

/**********************
 *  @brief Handles the request resource instruction
 *
 **********************/

void process_request (struct processControlBlock *current, struct instruction *instruct, struct resourceList **resource) //Handles the request resource instruction.
{
    struct processControlBlock *pcbIter = current, *p;
    struct resourceList *resourceIter = *(resource), *parent, *tmp;
    if (resource==NULL){
        printf("NULL resourceList given...\n Killing Program.\n");
        exit(0);
    }
    current->processState = RUNNING;
    set_process_ready(pcbIter->cpuSchedulePtr, pcbIter->pagePtr->number);
    parent = NULL;
    /**
     * if request and not in list... set to wait
     * else acquire
     * */
    if ( resourceIter != NULL) {
        while ( strcmp(resourceIter->name, instruct->resource) != 0 ) {
            parent = resourceIter;
            resourceIter = resourceIter->next;
            if (resourceIter == NULL) {
                proc_wait(current);
                printf("%s req %s: waiting;\n", current->pagePtr->name, current->nextInstruction->resource);
                return;
            }
        }
        if(parent!=NULL){
            if(resourceIter->next != NULL) parent->next = resourceIter->next;
            else parent->next = NULL;
        } else {
            if(resourceIter->next != NULL){
                *(resource)= resourceIter->next;
            } else {
                *(resource) = NULL;
            }
        }
        tmp = resourceIter;
        tmp->next = current->resourceListPtr;
        current->resourceListPtr = tmp;
    } else {
        proc_wait(current);
        printf("%s req %s: waiting;\n", current->pagePtr->name, current->nextInstruction->resource);
    }
    if(pcbIter->processState != WAITING){
        printf("%s req %s: acquired;", current->pagePtr->name, current->nextInstruction->resource);
        print_res(p, *(resource));
        printf("\n");
        current->nextInstruction = current->nextInstruction->next;
        dealloc_instruction(instruct);
    }
    return;
}

/**********************
 *  @brief Handles the release resource instruction
 *
 **********************/

void process_release (struct processControlBlock *current, struct instruction *instruct, struct resourceList **resource)
{
    struct processControlBlock *pcbIter, *p;
    struct resourceList *resourceIter;
    struct resourceList *parent, *tmp;

    if (resource==NULL){
        printf("NULL resourceList given...\n Killing Program.\n");
        exit(0);
    }
    current->processState = RUNNING;
    set_process_ready(current->cpuSchedulePtr, current->pagePtr->number);
    resourceIter = current->resourceListPtr;
    parent = NULL;
    if(resourceIter!=NULL){
        while ( strcmp(resourceIter->name, instruct->resource) != 0 ) {
            parent = resourceIter;
            resourceIter = resourceIter->next;
            if (resourceIter == NULL) {
                printf("Resource %s for release not allocated to Process %d\n", instruct->resource, current->pagePtr->number );
                exit(0);
                return;
            }
        }
        if (parent!=NULL) {
            if(resourceIter->next != NULL) parent->next = resourceIter->next;
            else parent->next = NULL;
        } else {
            if(resourceIter->next != NULL){
                current->resourceListPtr = current->resourceListPtr->next;
            } else {
                current->resourceListPtr = NULL;
            }
        }
        tmp = resourceIter;
        tmp->next = *(resource);
        *(resource) = tmp;
    } else {
        printf("Resource %s for release not allocated to Process %s\n", instruct->resource, current->pagePtr->name );
        exit(0);

    }
    printf("%s rel %s: released;", current->pagePtr->name, current->nextInstruction->resource);
    print_res(p, *(resource));
    printf("\n");
    current->nextInstruction = current->nextInstruction->next;
    dealloc_instruction(instruct);
    return;
}



/**
 * @brief Sends the message the prescribed mailbox.
 *
 * Sends the message specified in the instruction of the current process, to
 * the mailbox specified in the instruction. The function gets the pointer to
 * the first mailbox and loops through all the mailboxes to find the one to
 * which the message must be sent.
 *
 * @param pcb The current process which instruct us to send a message.
 * @param instruct The current send instruction which contains the message.
 * @param mail The list of available mailboxes.
 */

void process_send_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail) {
    struct mailbox *currentMbox;
    currentMbox = mail;
    if (mail==NULL){
        printf("NULL mailbox given...\n Killing Program.\n");
        exit(0);
    }
    do {
        if ( strcmp(currentMbox->name, instruct->resource) == 0 ) {
            /* We found the mailbox in which a message should be left */
            break;
        }
        currentMbox = currentMbox->next;
    } while ( currentMbox != NULL );


    if(currentMbox!=NULL){
        if( currentMbox->msg == NULL && pcb->processState == WAITING ) {
            pcb->processState = RUNNING;
            set_process_ready(pcb->cpuSchedulePtr, pcb->pagePtr->number);
            pcb->nextInstruction = pcb->nextInstruction->next;
            dealloc_instruction(instruct);
        } else if(currentMbox->msg!=NULL && strcmp(currentMbox->msg, pcb->nextInstruction->msg) != 0){
            /*
            printf("%s \033[22;31m FAILED \033[0m to store in %s\n Mailbox Full\n",
                    pcb->pagePtr->name, currentMbox->name);
                    */
            proc_wait(pcb);
        } else if(currentMbox->msg==NULL) {
            /*
            printf("%s sending message to mailbox %s which says \033[22;31m %s \033[0m\n",
                    pcb->pagePtr->name, currentMbox->name, instruct->msg);
                    */
            printf("%s send:\t\tMessage \"%s\" added to %s\n",pcb->pagePtr->name, instruct->msg, currentMbox->name);
            currentMbox->msg = instruct->msg;
            proc_wait(pcb);
        }
    } else{
        printf("MAILBOX EMPTY!\t\t\t[Killing]\n");
        exit(0);
    }
    return;
}
/**
 * @brief Retrieves the message from the mailbox specified in the instruction
 * and stores it in the instruction message field.
 *
 * The function loops through the available mailboxes and finds the mailbox
 * from which the message must be retrieved. The retrieved message is stored
 * in the message field of the instruction of the process.
 *
 * @param pcb The current process which requests a message retrieval.
 * @param instruct The instruction to retrieve a message from a specific
 * mailbox.
 * @param mail The list of mailboxes.
 */
void process_receive_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail) {

    struct mailbox *currentMbox;

    pcb->processState = RUNNING;
    set_process_ready(pcb->cpuSchedulePtr, pcb->pagePtr->number);
    if (mail==NULL){
        printf("NULL mailbox given...\n Killing Program.\n");
        exit(0);
    }

    currentMbox = mail;
    do {
        if ( strcmp(currentMbox->name, instruct->resource) == 0 ) {
            /* We found the mailbox from which a message must be read. */
            break;
        }
        currentMbox = currentMbox->next;
    } while ( currentMbox != NULL );
    if(strcmp(currentMbox->name, instruct->resource) !=0){
        printf("Mailbox not in list\t\t\t[Killing]\n");
        exit(0);
    }

    //printf("%s retrieved message from mailbox %s which says \033[22;32m %s \033[0m\n",
    //        pcb->pagePtr->name, currentMbox->name, currentMbox->msg);

    instruct->msg = currentMbox->msg;
    printf("%s recv:\t\tMessage \"%s\" removed from %s\n",pcb->pagePtr->name, instruct->msg, currentMbox->name);
    currentMbox->msg = NULL;
    pcb->nextInstruction = pcb->nextInstruction->next;
    dealloc_instruction(instruct);
}



/**
 * @brief   Sets the bit in the readyQueue for process proc.
 *
 * Makes use of the set_bit & clear_bit function, to set the bit for this process.
 *
 * @param   schedule The cpuSchedule struct for the process
 * @param   proc The number of the process
 */
void set_process_ready (struct cpuSchedule *schedule, int proc)
{

    set_bit(schedule->readyQueue, proc);
    clear_bit(schedule->waitingQueue, proc);
    clear_bit(schedule->terminatedQueue, proc);
    return;
}

/**
 * @brief Sets the bit in the waitingQueue for process proc.
 *
 * Makes use of the set_bit & clear_bit function, to set the bit for this process.
 *
 * @param schedule The cpuSchedule struct for the process
 * @param proc The number of the process
 */
void set_process_waiting (struct cpuSchedule *schedule, int proc)
{
    set_bit(schedule->waitingQueue, proc);
    clear_bit(schedule->readyQueue, proc);
    clear_bit(schedule->terminatedQueue, proc);
    return;
}

/**
 * @brief Sets the bit in the terminatedQueue for process proc.
 *
 * Makes use of the set_bit & clear_bit function, to set the bit for this process.
 *
 * @param schedule The cpuSchedule struct for the process
 * @param proc The number of the process
 */
void set_process_terminated (struct cpuSchedule *schedule, int proc)
{
    set_bit(schedule->terminatedQueue, proc);
    clear_bit(schedule->waitingQueue, proc);
    clear_bit(schedule->readyQueue, proc);
    return;
}

/**
 * @brief Sets a bit in the queue
 *
 * Uses the bit test and set function defined in asm.h to set a bit in a 32bit
 * integer.
 *
 * @param queue The array of ints in which the bit must be set.
 * @param proc The number of the process.
 */

void set_bit(int *queue, int proc)
{
    bts(queue[proc/INTBITSIZE], proc%INTBITSIZE);
}

/**
 * @brief Tests a bit in the queue
 *
 * Uses the bit test function defined in asm.h to check the status of a bit in
 * a 32 bit integer.
 *
 * @param queue The array of ints in which the bit must be set.
 * @param proc The number of the process.
 *
 * @return The observed status of the bit.
 */
int read_bit(int *queue, int proc)
{
    return bt(queue[proc/INTBITSIZE], proc%INTBITSIZE);
}

/**
 * @brief Resets a bit in the queue
 *
 * Uses the bit test and reset function defined in asm.h to clear the bit
 * specified by the proc in a 32 bit integer.
 *
 * @param queue The array of ints in which the bit must be set.
 * @param proc The number of the process.
 */
void clear_bit(int *queue, int proc)
{
    btr(queue[proc/INTBITSIZE], proc%INTBITSIZE);
}

/**
 * @brief
 *
 *
 *
 */
void proc_wait (struct processControlBlock *p){
    p->processState = WAITING;
    set_process_waiting(p->cpuSchedulePtr, p->pagePtr->number);
    /*
    if(p->nextInstruction->type!=2){
        if (p->nextInstruction->resource != NULL) {
            printf("\t%s, set to WAITING\nResource: %s", p->pagePtr->name, p->nextInstruction->resource);
        }
        else {
            printf("\t%s, set to WAITING\nMessage: %s", p->pagePtr->name, p->nextInstruction->msg);
        }
    } else {
        printf("\n Message sent!\n%s waiting for receive.\n", p->pagePtr->name);
    }
    */

    return;
}

/**
 * @brief
 *
 *
 *
 */
int processes_finished (struct processControlBlock *firstPCB)
{
    struct processControlBlock *pcbIter;
    pcbIter = firstPCB;
    do {
            if(pcbIter->processState==2|| pcbIter->processState==3 || boole ==0){
                return 0;
            }
            pcbIter = pcbIter->next;
    } while(pcbIter!=NULL);
    return 1;
}

/**
 * @brief
 *
 *
 */
int processes_deadlocked (struct processControlBlock *firstPCB)
{


    int waiting = *(firstPCB->cpuSchedulePtr->waitingQueue);
    int terminated = *(firstPCB->cpuSchedulePtr->terminatedQueue);
    int fin = 1;
    struct processControlBlock *pcbIter = firstPCB;

    do{
        if (pcbIter->processState==2){
            fin = 0;
        }
        pcbIter = pcbIter->next;
    }while(pcbIter!=NULL);
    /*
    printf("\n%d, %d\n", glob_check, local_check);
    printf("\n%d\n", firstPCB->cpuSchedulePtr->readyQueue);
    */
    if((prev_terminator == terminated ) && (prev_waiting == waiting) && 0 == processes_finished(firstPCB) && fin == 1){ // && boole !=0
        printf("Processes Deadlocked!\t\t\t\t[Killing]\n");
        exit(0);
        return 1;
    } else {
        prev_terminator = terminated;
        prev_waiting = waiting;
    }
    return 0;
}

/**
 * @brief
 *
 *
 void process_release (struct processControlBlock *current, struct instruction *instruct, struct resourceList *resource); // Handles the release resource instruction
 void process_request (struct processControlBlock *current, struct instruction *instruct, struct resourceList *resource); //Handles the request resource instruction.
 void process_send_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail);
 void process_receive_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail);
 */
void schedule_processes (struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail, int FCFS, int quant)
{

    /*
     *Add Check function to kill while
     * */
    prev_waiting = -1;
    prev_terminator = -1;
    if(quant>0) quantum = quant;
    else quantum = 1;
    if(FCFS){
        printf("\n*************************************************************\nFirst Come, First Serve protocol implemenetation\n*************************************************************\n");
        fcfs(pcb, resource, mail);
    }
    else{
        printf("\n*************************************************************\n Round Robin protocol implemenetation\n*************************************************************\n");
        round_robin(pcb, resource, mail);
    }
    printf("\n*************\n*Program End*\n*************\n");
    return;
}
void fcfs(struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail) {
    struct processControlBlock *pcb_save, *p;
    struct instruction *curr_instruct;
    pcb_save = pcb;
    curr_instruct = pcb->nextInstruction;
    res_check(pcb, resource, mail);
    for(boole =0; 0 == processes_finished(pcb); boole++ ) { // && !processes_deadlocked(pcb)*/
        do {
            while (pcb->processState!=TERMINATED ) {
                curr_instruct = pcb->nextInstruction;
                // printf("\n 1:\t%s -> %d State %d\n", pcb->pagePtr->name, pcb->nextInstruction->type, pcb->processState);
                if ( curr_instruct == NULL ) {
                    pcb->processState = TERMINATED;
                    set_process_terminated(pcb->cpuSchedulePtr, pcb->pagePtr->number);
                    printf("%s terminated\n", pcb->pagePtr->name);
                    break;
                }
                switch (curr_instruct->type){
                    case 0:
                        process_request (pcb, curr_instruct, &(resource));
                        break;//
                    case 1:
                        process_release (pcb, curr_instruct, &(resource));
                        break;//
                    case 2:
                        process_send_message(pcb, curr_instruct, mail);
                        break;//
                    case 3:
                        process_receive_message(pcb, curr_instruct, mail);
                        break;//
                    default:
                        printf("\nNo valid instruction type <0-3>, returned type was of int %s\t %s\n", pcb->nextInstruction->resource, pcb->pagePtr->name);
                        exit(0);
                        break;
                }
                //printf("\n 2:\t%s -> %d State %d\n", pcb->pagePtr->name, pcb->nextInstruction->type, pcb->processState);
                curr_instruct =pcb->nextInstruction;
                if ( curr_instruct == NULL ) {
                    pcb->processState = TERMINATED;
                    set_process_terminated(pcb->cpuSchedulePtr, pcb->pagePtr->number);
                    printf("%s terminated\n", pcb->pagePtr->name);
                    break;
                }
                if ( pcb->processState == WAITING ) {
                    break;
                }
            }
            pcb =pcb->next;
        } while (pcb!=NULL);
        pcb = pcb_save;
        if(processes_deadlocked(pcb)){
            break;
        }
    }
}

/**
 *  @brief Round Robin Implementation
 *
 *  Implements a round robin algorithm for process Scheduling based on a given time quantum, set in the main method, or defaults to 1;
 *
 */

void round_robin(struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail) {
    struct processControlBlock *pcb_save, *p;
    struct instruction *curr_instruct;
    int i;
    pcb_save = pcb;
    curr_instruct = pcb->nextInstruction;
    res_check(pcb, resource, mail);
    for(boole =0; 0 == processes_finished(pcb); boole++ ) {
        do {
            for(i =0; i< quantum; i++){
                if (pcb->processState!=TERMINATED ) {
                    curr_instruct = pcb->nextInstruction;
                    if ( curr_instruct == NULL ) {
                        pcb->processState = TERMINATED;
                        set_process_terminated(pcb->cpuSchedulePtr, pcb->pagePtr->number);
                        printf("%s terminated\n", pcb->pagePtr->name);
                        break;
                    }
                    switch (curr_instruct->type){
                        case 0:
                            process_request (pcb, curr_instruct, &(resource));
                            break;//
                        case 1:
                            process_release (pcb, curr_instruct, &(resource));
                            break;//
                        case 2:
                            process_send_message(pcb, curr_instruct, mail);
                            break;//
                        case 3:
                            process_receive_message(pcb, curr_instruct, mail);
                            break;//
                        default:
                            printf("\nNo valid instruction type <0-3>, returned type was of int %s\t %s\n", pcb->nextInstruction->resource, pcb->pagePtr->name);
                            exit(0);
                            break;
                    }
                    if ( curr_instruct == NULL ) {
                        pcb->processState = TERMINATED;
                        set_process_terminated(pcb->cpuSchedulePtr, pcb->pagePtr->number);
                        printf("%s terminated\n", pcb->pagePtr->name);
                        break;
                    }
                    if ( pcb->processState == WAITING ) {
                        break;
                    }
                }
            }
            pcb =pcb->next;
        } while (pcb!=NULL);
        pcb = pcb_save;
        if(processes_deadlocked(pcb)){
            break;
        }
    }
}

void print_res(struct processControlBlock *p, struct resourceList *res){
    struct resourceList *tmp = res;
    if(p!=NULL){
        printf("\n----------------------\n\tResourceList\n----------------------\n");
        while(tmp!=NULL){
            if(p->nextInstruction->type == 0){
                //printf("\n%s req %s \n", p->pagePtr->name,tmp->name );
            } else if(p->nextInstruction->type == 1){
                //printf("\n%s rel %s \n", p->pagePtr->name,tmp->name );
            }
            tmp = tmp->next;
        }
    }
    else {
        printf("Available: ");
        while(tmp!=NULL){
            printf("%s ", tmp->name);
            tmp = tmp->next;
        }

    }

}
void res_check(struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail) {
        if (pcb==NULL){
            printf("NULL processControlBlock given...\n Killing Program.\n");
            exit(0);
        }
}
