/**
 * @file manager.h
 */
#ifndef _MANAGER_H
#define _MANAGER_H

#include "loader.h"
#include "asm.h"

void process_release (struct processControlBlock *current, struct instruction *instruct, struct resourceList **resource); //Handles the release resource instruction
void process_request (struct processControlBlock *current, struct instruction *instruct, struct resourceList **resource); //Handles the request resource instruction.
void process_send_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail);
void process_receive_message(struct processControlBlock *pcb, struct instruction *instruct, struct mailbox *mail);
int acquire_resource (char *resourceName, struct resourceList *resources, struct processControlBlock *p); //Acquires the resource specied by resourceName.
int release_resource (char *resourceName, struct resourceList *resources, struct processControlBlock *p); //Releases the resource specied by resourceName.
void add_resource_to_process (struct processControlBlock *current, struct resourceList *resource); //Adds the specied resource to the process acquired resource list
void release_resource_from_process (struct processControlBlock *current, struct resourceList *resource); //Release the specied resource from the process acquired list.
void set_process_ready (struct cpuSchedule *schedule, int proc); //Sets the bit in the readyQueue for process proc.
void set_process_waiting (struct cpuSchedule *schedule, int proc); //Sets the bit in the waitingQueue for process proc.
void set_process_terminated (struct cpuSchedule *schedule, int proc); //Sets the bit in the terminatedQueue for process proc.
void set_bit (int *queue, int proc); //Sets a bit in the queue.
int read_bit (int *queue, int proc); //Tests a bit in the queue.
void clear_bit (int *queue, int proc); //Resets a bit in the queue.
void proc_wait (struct processControlBlock *p);
int processes_finished (struct processControlBlock *firstPCB); //Iterates over each of the loaded processes and checks if it has been terminated.
int processes_deadlocked (struct processControlBlock *firstPCB); // Iterates over each of the loaded process and check if all the processes are deadlocked.
void schedule_processes (struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail, int FCFS, int quant); //Schedules each instruction of each process in a round-robin fashion. The number of instruction to execute for each process is governed by the QUANTUM variable.

void res_check(struct processControlBlock *pcb, struct resourceList *resource, struct mailbox *mail);

void print_res(struct processControlBlock *p, struct resourceList *res);
#endif
