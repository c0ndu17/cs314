\contentsline {chapter}{\numberline {1}\discretionary {-}{}{}Process \discretionary {-}{}{}Simulation}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}\discretionary {-}{}{}Introduction}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}\discretionary {-}{}{}Compile}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}\discretionary {-}{}{}Execute}{1}{section.1.3}
\contentsline {chapter}{\numberline {2}\discretionary {-}{}{}Data \discretionary {-}{}{}Structure \discretionary {-}{}{}Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}\discretionary {-}{}{}Data \discretionary {-}{}{}Structures}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}\discretionary {-}{}{}File \discretionary {-}{}{}Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}\discretionary {-}{}{}File \discretionary {-}{}{}List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}\discretionary {-}{}{}Data \discretionary {-}{}{}Structure \discretionary {-}{}{}Documentation}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}cpu\discretionary {-}{}{}Schedule \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{7}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{7}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}\discretionary {-}{}{}Field \discretionary {-}{}{}Documentation}{7}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}process\discretionary {-}{}{}Priority}{7}{subsubsection.4.1.2.1}
\contentsline {subsubsection}{\numberline {4.1.2.2}ready\discretionary {-}{}{}Queue}{7}{subsubsection.4.1.2.2}
\contentsline {subsubsection}{\numberline {4.1.2.3}terminated\discretionary {-}{}{}Queue}{8}{subsubsection.4.1.2.3}
\contentsline {subsubsection}{\numberline {4.1.2.4}waiting\discretionary {-}{}{}Queue}{8}{subsubsection.4.1.2.4}
\contentsline {section}{\numberline {4.2}instruction \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{8}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{8}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}\discretionary {-}{}{}Field \discretionary {-}{}{}Documentation}{8}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}msg}{8}{subsubsection.4.2.2.1}
\contentsline {subsubsection}{\numberline {4.2.2.2}next}{8}{subsubsection.4.2.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.3}resource}{8}{subsubsection.4.2.2.3}
\contentsline {subsubsection}{\numberline {4.2.2.4}type}{9}{subsubsection.4.2.2.4}
\contentsline {section}{\numberline {4.3}mailbox \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{9}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{9}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}\discretionary {-}{}{}Field \discretionary {-}{}{}Documentation}{9}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}msg}{9}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}name}{9}{subsubsection.4.3.2.2}
\contentsline {subsubsection}{\numberline {4.3.2.3}next}{9}{subsubsection.4.3.2.3}
\contentsline {section}{\numberline {4.4}page \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{9}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{10}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}\discretionary {-}{}{}Field \discretionary {-}{}{}Documentation}{10}{subsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.2.1}name}{10}{subsubsection.4.4.2.1}
\contentsline {subsubsection}{\numberline {4.4.2.2}number}{10}{subsubsection.4.4.2.2}
\contentsline {section}{\numberline {4.5}process\discretionary {-}{}{}Control\discretionary {-}{}{}Block \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{10}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{10}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}\discretionary {-}{}{}Field \discretionary {-}{}{}Documentation}{11}{subsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.2.1}cpu\discretionary {-}{}{}Schedule\discretionary {-}{}{}Ptr}{11}{subsubsection.4.5.2.1}
\contentsline {subsubsection}{\numberline {4.5.2.2}next}{11}{subsubsection.4.5.2.2}
\contentsline {subsubsection}{\numberline {4.5.2.3}next\discretionary {-}{}{}Instruction}{11}{subsubsection.4.5.2.3}
\contentsline {subsubsection}{\numberline {4.5.2.4}page\discretionary {-}{}{}Ptr}{11}{subsubsection.4.5.2.4}
\contentsline {subsubsection}{\numberline {4.5.2.5}process\discretionary {-}{}{}State}{11}{subsubsection.4.5.2.5}
\contentsline {subsubsection}{\numberline {4.5.2.6}resource\discretionary {-}{}{}List\discretionary {-}{}{}Ptr}{11}{subsubsection.4.5.2.6}
\contentsline {section}{\numberline {4.6}resource\discretionary {-}{}{}List \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{11}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{12}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}\discretionary {-}{}{}Field \discretionary {-}{}{}Documentation}{12}{subsection.4.6.2}
\contentsline {subsubsection}{\numberline {4.6.2.1}available}{12}{subsubsection.4.6.2.1}
\contentsline {subsubsection}{\numberline {4.6.2.2}name}{12}{subsubsection.4.6.2.2}
\contentsline {subsubsection}{\numberline {4.6.2.3}next}{12}{subsubsection.4.6.2.3}
\contentsline {chapter}{\numberline {5}\discretionary {-}{}{}File \discretionary {-}{}{}Documentation}{13}{chapter.5}
\contentsline {section}{\numberline {5.1}loader.c \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{13}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{14}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{14}{subsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.2.1}dealloc\discretionary {-}{}{}\_\discretionary {-}{}{}cpu\discretionary {-}{}{}Schedule}{14}{subsubsection.5.1.2.1}
\contentsline {subsubsection}{\numberline {5.1.2.2}dealloc\discretionary {-}{}{}\_\discretionary {-}{}{}instruction}{15}{subsubsection.5.1.2.2}
\contentsline {subsubsection}{\numberline {5.1.2.3}dealloc\discretionary {-}{}{}\_\discretionary {-}{}{}mailboxes}{15}{subsubsection.5.1.2.3}
\contentsline {subsubsection}{\numberline {5.1.2.4}dealloc\discretionary {-}{}{}\_\discretionary {-}{}{}page}{15}{subsubsection.5.1.2.4}
\contentsline {subsubsection}{\numberline {5.1.2.5}dealloc\discretionary {-}{}{}\_\discretionary {-}{}{}processes}{15}{subsubsection.5.1.2.5}
\contentsline {subsubsection}{\numberline {5.1.2.6}dealloc\discretionary {-}{}{}\_\discretionary {-}{}{}queues}{15}{subsubsection.5.1.2.6}
\contentsline {subsubsection}{\numberline {5.1.2.7}dealloc\discretionary {-}{}{}\_\discretionary {-}{}{}resource\discretionary {-}{}{}List}{15}{subsubsection.5.1.2.7}
\contentsline {subsubsection}{\numberline {5.1.2.8}get\discretionary {-}{}{}\_\discretionary {-}{}{}available\discretionary {-}{}{}\_\discretionary {-}{}{}resources}{15}{subsubsection.5.1.2.8}
\contentsline {subsubsection}{\numberline {5.1.2.9}get\discretionary {-}{}{}\_\discretionary {-}{}{}loaded\discretionary {-}{}{}\_\discretionary {-}{}{}processes}{16}{subsubsection.5.1.2.9}
\contentsline {subsubsection}{\numberline {5.1.2.10}get\discretionary {-}{}{}\_\discretionary {-}{}{}mailboxes}{16}{subsubsection.5.1.2.10}
\contentsline {subsubsection}{\numberline {5.1.2.11}load\discretionary {-}{}{}\_\discretionary {-}{}{}mailbox}{16}{subsubsection.5.1.2.11}
\contentsline {subsubsection}{\numberline {5.1.2.12}load\discretionary {-}{}{}\_\discretionary {-}{}{}process}{16}{subsubsection.5.1.2.12}
\contentsline {subsubsection}{\numberline {5.1.2.13}load\discretionary {-}{}{}\_\discretionary {-}{}{}process\discretionary {-}{}{}\_\discretionary {-}{}{}instruction}{17}{subsubsection.5.1.2.13}
\contentsline {subsubsection}{\numberline {5.1.2.14}load\discretionary {-}{}{}\_\discretionary {-}{}{}resource}{17}{subsubsection.5.1.2.14}
\contentsline {subsubsection}{\numberline {5.1.2.15}ready\discretionary {-}{}{}\_\discretionary {-}{}{}queue}{17}{subsubsection.5.1.2.15}
\contentsline {subsubsection}{\numberline {5.1.2.16}terminated\discretionary {-}{}{}\_\discretionary {-}{}{}queue}{17}{subsubsection.5.1.2.16}
\contentsline {subsubsection}{\numberline {5.1.2.17}waiting\discretionary {-}{}{}\_\discretionary {-}{}{}queue}{18}{subsubsection.5.1.2.17}
\contentsline {section}{\numberline {5.2}loader.h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{18}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{19}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}\discretionary {-}{}{}Define \discretionary {-}{}{}Documentation}{19}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}\discretionary {-}{}{}N\discretionary {-}{}{}E\discretionary {-}{}{}W}{19}{subsubsection.5.2.2.1}
\contentsline {subsubsection}{\numberline {5.2.2.2}\discretionary {-}{}{}R\discretionary {-}{}{}E\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}Y}{19}{subsubsection.5.2.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.3}\discretionary {-}{}{}R\discretionary {-}{}{}U\discretionary {-}{}{}N\discretionary {-}{}{}N\discretionary {-}{}{}I\discretionary {-}{}{}N\discretionary {-}{}{}G}{19}{subsubsection.5.2.2.3}
\contentsline {subsubsection}{\numberline {5.2.2.4}\discretionary {-}{}{}T\discretionary {-}{}{}E\discretionary {-}{}{}R\discretionary {-}{}{}M\discretionary {-}{}{}I\discretionary {-}{}{}N\discretionary {-}{}{}A\discretionary {-}{}{}T\discretionary {-}{}{}E\discretionary {-}{}{}D}{19}{subsubsection.5.2.2.4}
\contentsline {subsubsection}{\numberline {5.2.2.5}\discretionary {-}{}{}W\discretionary {-}{}{}A\discretionary {-}{}{}I\discretionary {-}{}{}T\discretionary {-}{}{}I\discretionary {-}{}{}N\discretionary {-}{}{}G}{19}{subsubsection.5.2.2.5}
\contentsline {subsection}{\numberline {5.2.3}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{20}{subsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.3.1}dealloc\discretionary {-}{}{}\_\discretionary {-}{}{}instruction}{20}{subsubsection.5.2.3.1}
\contentsline {subsubsection}{\numberline {5.2.3.2}dealloc\discretionary {-}{}{}\_\discretionary {-}{}{}processes}{20}{subsubsection.5.2.3.2}
\contentsline {subsubsection}{\numberline {5.2.3.3}get\discretionary {-}{}{}\_\discretionary {-}{}{}available\discretionary {-}{}{}\_\discretionary {-}{}{}resources}{20}{subsubsection.5.2.3.3}
\contentsline {subsubsection}{\numberline {5.2.3.4}get\discretionary {-}{}{}\_\discretionary {-}{}{}loaded\discretionary {-}{}{}\_\discretionary {-}{}{}processes}{20}{subsubsection.5.2.3.4}
\contentsline {subsubsection}{\numberline {5.2.3.5}get\discretionary {-}{}{}\_\discretionary {-}{}{}mailboxes}{20}{subsubsection.5.2.3.5}
\contentsline {subsubsection}{\numberline {5.2.3.6}load\discretionary {-}{}{}\_\discretionary {-}{}{}mailbox}{21}{subsubsection.5.2.3.6}
\contentsline {subsubsection}{\numberline {5.2.3.7}load\discretionary {-}{}{}\_\discretionary {-}{}{}process}{21}{subsubsection.5.2.3.7}
\contentsline {subsubsection}{\numberline {5.2.3.8}load\discretionary {-}{}{}\_\discretionary {-}{}{}process\discretionary {-}{}{}\_\discretionary {-}{}{}instruction}{21}{subsubsection.5.2.3.8}
\contentsline {subsubsection}{\numberline {5.2.3.9}load\discretionary {-}{}{}\_\discretionary {-}{}{}resource}{21}{subsubsection.5.2.3.9}
\contentsline {section}{\numberline {5.3}main.c \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{22}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{22}{subsection.5.3.1}
\contentsline {section}{\numberline {5.4}manager.c \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{22}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{24}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{24}{subsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.2.1}acquire\discretionary {-}{}{}\_\discretionary {-}{}{}resource}{24}{subsubsection.5.4.2.1}
\contentsline {subsubsection}{\numberline {5.4.2.2}add\discretionary {-}{}{}\_\discretionary {-}{}{}resource\discretionary {-}{}{}\_\discretionary {-}{}{}to\discretionary {-}{}{}\_\discretionary {-}{}{}process}{24}{subsubsection.5.4.2.2}
\contentsline {subsubsection}{\numberline {5.4.2.3}clear\discretionary {-}{}{}\_\discretionary {-}{}{}bit}{24}{subsubsection.5.4.2.3}
\contentsline {subsubsection}{\numberline {5.4.2.4}process\discretionary {-}{}{}\_\discretionary {-}{}{}receive\discretionary {-}{}{}\_\discretionary {-}{}{}message}{25}{subsubsection.5.4.2.4}
\contentsline {subsubsection}{\numberline {5.4.2.5}process\discretionary {-}{}{}\_\discretionary {-}{}{}release}{25}{subsubsection.5.4.2.5}
\contentsline {subsubsection}{\numberline {5.4.2.6}process\discretionary {-}{}{}\_\discretionary {-}{}{}request}{25}{subsubsection.5.4.2.6}
\contentsline {subsubsection}{\numberline {5.4.2.7}process\discretionary {-}{}{}\_\discretionary {-}{}{}send\discretionary {-}{}{}\_\discretionary {-}{}{}message}{26}{subsubsection.5.4.2.7}
\contentsline {subsubsection}{\numberline {5.4.2.8}processes\discretionary {-}{}{}\_\discretionary {-}{}{}deadlocked}{26}{subsubsection.5.4.2.8}
\contentsline {subsubsection}{\numberline {5.4.2.9}processes\discretionary {-}{}{}\_\discretionary {-}{}{}finished}{26}{subsubsection.5.4.2.9}
\contentsline {subsubsection}{\numberline {5.4.2.10}read\discretionary {-}{}{}\_\discretionary {-}{}{}bit}{26}{subsubsection.5.4.2.10}
\contentsline {subsubsection}{\numberline {5.4.2.11}release\discretionary {-}{}{}\_\discretionary {-}{}{}resource}{27}{subsubsection.5.4.2.11}
\contentsline {subsubsection}{\numberline {5.4.2.12}release\discretionary {-}{}{}\_\discretionary {-}{}{}resource\discretionary {-}{}{}\_\discretionary {-}{}{}from\discretionary {-}{}{}\_\discretionary {-}{}{}process}{27}{subsubsection.5.4.2.12}
\contentsline {subsubsection}{\numberline {5.4.2.13}schedule\discretionary {-}{}{}\_\discretionary {-}{}{}processes}{27}{subsubsection.5.4.2.13}
\contentsline {subsubsection}{\numberline {5.4.2.14}set\discretionary {-}{}{}\_\discretionary {-}{}{}bit}{28}{subsubsection.5.4.2.14}
\contentsline {subsubsection}{\numberline {5.4.2.15}set\discretionary {-}{}{}\_\discretionary {-}{}{}process\discretionary {-}{}{}\_\discretionary {-}{}{}ready}{28}{subsubsection.5.4.2.15}
\contentsline {subsubsection}{\numberline {5.4.2.16}set\discretionary {-}{}{}\_\discretionary {-}{}{}process\discretionary {-}{}{}\_\discretionary {-}{}{}terminated}{28}{subsubsection.5.4.2.16}
\contentsline {subsubsection}{\numberline {5.4.2.17}set\discretionary {-}{}{}\_\discretionary {-}{}{}process\discretionary {-}{}{}\_\discretionary {-}{}{}waiting}{29}{subsubsection.5.4.2.17}
\contentsline {section}{\numberline {5.5}manager.h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{29}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{29}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{29}{subsection.5.5.2}
\contentsline {subsubsection}{\numberline {5.5.2.1}schedule\discretionary {-}{}{}\_\discretionary {-}{}{}processes}{29}{subsubsection.5.5.2.1}
\contentsline {section}{\numberline {5.6}parser.c \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{30}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{30}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{30}{subsection.5.6.2}
\contentsline {subsubsection}{\numberline {5.6.2.1}open\discretionary {-}{}{}\_\discretionary {-}{}{}process\discretionary {-}{}{}\_\discretionary {-}{}{}file}{30}{subsubsection.5.6.2.1}
\contentsline {subsubsection}{\numberline {5.6.2.2}parse\discretionary {-}{}{}\_\discretionary {-}{}{}process\discretionary {-}{}{}\_\discretionary {-}{}{}file}{31}{subsubsection.5.6.2.2}
\contentsline {subsubsection}{\numberline {5.6.2.3}read\discretionary {-}{}{}\_\discretionary {-}{}{}comms\discretionary {-}{}{}\_\discretionary {-}{}{}recv}{31}{subsubsection.5.6.2.3}
\contentsline {subsubsection}{\numberline {5.6.2.4}read\discretionary {-}{}{}\_\discretionary {-}{}{}comms\discretionary {-}{}{}\_\discretionary {-}{}{}send}{31}{subsubsection.5.6.2.4}
\contentsline {subsubsection}{\numberline {5.6.2.5}read\discretionary {-}{}{}\_\discretionary {-}{}{}mailboxes}{32}{subsubsection.5.6.2.5}
\contentsline {subsubsection}{\numberline {5.6.2.6}read\discretionary {-}{}{}\_\discretionary {-}{}{}process}{32}{subsubsection.5.6.2.6}
\contentsline {subsubsection}{\numberline {5.6.2.7}read\discretionary {-}{}{}\_\discretionary {-}{}{}processes}{32}{subsubsection.5.6.2.7}
\contentsline {subsubsection}{\numberline {5.6.2.8}read\discretionary {-}{}{}\_\discretionary {-}{}{}rel\discretionary {-}{}{}\_\discretionary {-}{}{}resource}{33}{subsubsection.5.6.2.8}
\contentsline {subsubsection}{\numberline {5.6.2.9}read\discretionary {-}{}{}\_\discretionary {-}{}{}req\discretionary {-}{}{}\_\discretionary {-}{}{}resource}{33}{subsubsection.5.6.2.9}
\contentsline {subsubsection}{\numberline {5.6.2.10}read\discretionary {-}{}{}\_\discretionary {-}{}{}resources}{33}{subsubsection.5.6.2.10}
\contentsline {subsubsection}{\numberline {5.6.2.11}read\discretionary {-}{}{}\_\discretionary {-}{}{}string}{33}{subsubsection.5.6.2.11}
\contentsline {section}{\numberline {5.7}parser.h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{34}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{34}{subsection.5.7.1}
\contentsline {subsection}{\numberline {5.7.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{34}{subsection.5.7.2}
\contentsline {subsubsection}{\numberline {5.7.2.1}parse\discretionary {-}{}{}\_\discretionary {-}{}{}process\discretionary {-}{}{}\_\discretionary {-}{}{}file}{34}{subsubsection.5.7.2.1}
\contentsline {section}{\numberline {5.8}syntax.h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{34}{section.5.8}
\contentsline {subsection}{\numberline {5.8.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{35}{subsection.5.8.1}
