
#include <stdio.h>
#include <string.h>
#include <mpi.h>

const int MAX_STRING = 100 ;
int main(void) {
    int comm_sz;
    int my_rank;
    int i;
    int out_rank;
    int in_rank;
    int ranksum;
    int RankOfProc;
    int RetRank;

    MPI_Init(NULL,NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    RankOfProc = my_rank;
    if (my_rank == 0) {
        out_rank = 1;
        in_rank = comm_sz - 1;
    } else if (my_rank == comm_sz -1){
        out_rank = 0;
        in_rank = my_rank - 1;
    } else {
        out_rank = my_rank + 1;
        in_rank = my_rank - 1;
    }

    for( i =1; i < comm_sz; i++){
        MPI_Sendrecv(&RankOfProc, 1, MPI_INTEGER, out_rank, 1, &RetRank, 1, MPI_INTEGER, in_rank, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        ranksum = ranksum + RetRank;
        RankOfProc = RetRank;
    }


    if(my_rank ==0){
        printf("rank sum = %d", ranksum);

    }

    /* Shut down MPI */
    MPI_Finalize();
    return 0;
}
