
#include <stdio.h>
#include <string.h>
#include <mpi.h>

const int MAX_STRING = 100 ;
int main(void) {
    int comm_sz;
    int my_rank;
    int i;
    int rank_sum;

    MPI_Init(NULL,NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    MPI_Allreduce(&my_rank, &rank_sum, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD);

    if(my_rank == 0 )
        printf("rank sum = %d\n", ranksum);
    /* Shut down MPI */
    MPI_Finalize();
    return 0;
}
