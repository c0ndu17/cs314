if [ $# -ne 3]; then
    echo "Usage: <filename> [v]"
    exit 0
else 
    echo "Serial Quick"
    time ./obj/Serial_quick test/$2 $3
    echo "Parallel_Merge"
    time mpirun -p $1 ./obj/Parallel_Merge test/$2 $3
    echo "Parallel Quick"
    time mpirun -p $1 ./obj/Parallel_Quick test/$2 $3
fi
exit(0)
