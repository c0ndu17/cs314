#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <time.h>

#define MAX_LENGTH 200

void quick_sort(int *temp, int n);

FILE *src_file;

int main (int argc, char* argv[])
{
    int *data = malloc(sizeof(int)*MAX_LENGTH);
    int size;
    int numEl;
    char* next_num;
    char* verbose = "v";
    int i = 0;
    double n;
    clock_t start, finish;
    if(argc<1){
        printf("<usage>: ./Serial_quick filename [optional print]");
        return EXIT_FAILURE;
    }
    size = 0;
    fprintf(stderr,"Reading data from: %s\n", argv[1]);
    if((src_file = fopen(argv[1],"r"))==NULL){
        printf("input.txt could not be read");
        return EXIT_FAILURE;
    }
    while((fscanf(src_file, "%d", &(numEl)))>0){
        size++;
    }
    fclose(src_file);
    data = (int *)malloc( sizeof(int) * size );
    if((src_file = fopen(argv[1],"r"))==NULL){
        printf("input.txt could not be read");
        return EXIT_FAILURE;
    }
    for( i =0; i<size; i++){
        fscanf(src_file, "%d", &data[i]);
    }
    fclose(src_file);
    size--;

    start = clock();
    quick_sort(data, size);
    finish = clock();
    n = (double)(finish - start)/CLOCKS_PER_SEC;
    printf("%f", n);

    if((argc==3 )&& (strcmp(argv[2], verbose)==0)){
        for (i =0; i< size; i++){
            printf("%d\n", data[i]);
        }
    }

    return EXIT_SUCCESS;
    /*
     * Read n *
     fscanf(fp, "%d", n_p);
     if ((*n_p <= 0) || (*n_p % comm_sz != 0)) local_ok = 0;
     check_for_error(local_ok, fname,
     "n should be > 0 and evenly divisible by comm_sz", comm);
     * Read vector a *
     a = malloc((*n_p)*sizeof(int));
     if (a == NULL) local_ok = 0;
     check_for_error(local_ok, fname,
     "Can't allocate temporary vector", comm);
     for (i = 0; i < *n_p; i++)
     fscanf(fp, "%d", &a[i]);
     */


}
void quick_sort (int *temp, int n) {
    if (n < 2)
        return;
    int t;
    int p = temp[n / 2];
    int *l = temp;
    int *r = temp + n - 1;
    while (l <= r) {
        if (*l < p) {
            l++;
            continue;
        }
        if (*r > p) {
            r--;
            continue;
        }
        t = *l;
        *l++ = *r;
        *r-- = t;
    }
    quick_sort(temp, r - temp + 1);
    quick_sort(l, temp + n - l);
    return;

}
