#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>

int scatter();
int proc_splitter();
void print_selection();
void quick_slave(int numEl);
int stat_receive();
void gather_data();
void quick_sort (int *temp, int n);
void merge (int lo, int mid, int hi);

FILE *src_file;
int *data;
int *tmp ;
int size;
int comm_sz;
int my_rank;
int section_size;

int main (int argc, char* argv[])
{
    int i, numEl;
    double starttime, endtime;
    char *verbose = "v";
    MPI_Init(NULL,NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    if(my_rank == 0 && argc<1){
        printf("<usage>: ./SerialQuick filename [optional print]");
        return EXIT_FAILURE;
    }
    if ( my_rank == 0) {

        size = 0;
        if((src_file = fopen(argv[1],"r"))==NULL){
            printf("input.txt could not be read");
            return EXIT_FAILURE;
        }
        while((fscanf(src_file, "%d", &(numEl)))>0){
            size++;
        }
        fclose(src_file);
        data = (int *)malloc( sizeof(int) * size );
        if((src_file = fopen(argv[1],"r"))==NULL){
            printf("input.txt could not be read");
            return EXIT_FAILURE;
        }
        for( i =0; i<size; i++){
            fscanf(src_file, "%d", &data[i]);
        }
        fclose(src_file);
        size--;

        //printf("\nData read...\n");
        //printf("\nProceeding to sort\n");
    }
    starttime = MPI_Wtime();
    proc_splitter();
    numEl=comm_sz/2;
    while (numEl > 0){
        quick_slave(numEl);
        numEl /= 2;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if(my_rank==0){
        endtime = MPI_Wtime();
        for(i = 0; i< size; i++){
            data[i] = tmp[i];
        }
        if(argc==3 && (strcmp(argv[2], verbose)==0)){
            for (i =0; i< size; i++){
                printf("%d\n",data[i]);
            }
        }
        printf("That took %f seconds\n", endtime-starttime);
    }
    free(tmp);
    free(data);
    MPI_Finalize();
    return EXIT_SUCCESS;

}


int proc_splitter () {
    stat_receive();
    MPI_Barrier(MPI_COMM_WORLD);
    quick_sort(tmp, section_size);
    gather_data();
    return 0;

}

int stat_receive(){
    int sendcount[comm_sz];
    int displacement[comm_sz];
    int i;
    if(tmp!=NULL)free(tmp);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    section_size =  size/comm_sz;
    if (my_rank == 0){
        for ( i =0; i< comm_sz-1; i++){
            displacement[i] = i*section_size;
            sendcount[i] = section_size;
        }
        displacement[comm_sz-1] = i*section_size;
        sendcount[comm_sz-1] = size-(section_size*(comm_sz-1));
    } else if(my_rank == comm_sz-1)
        section_size = size-(section_size*(comm_sz-1));
    tmp = (int *) malloc( section_size*sizeof(int));
    MPI_Barrier (MPI_COMM_WORLD);
    MPI_Scatterv(data, sendcount, displacement, MPI_INT, tmp, section_size, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    return 0;
}
void gather_data () {
    int i;
    int *recvcount = (int *)malloc(sizeof(int)*comm_sz);
    int *displacement = (int *)malloc(sizeof(int)*comm_sz);
    if (my_rank == 0){
        for ( i =0; i< comm_sz-1; i++){
            displacement[i] = i*section_size;
            recvcount[i] = section_size;
        }
        displacement[comm_sz-1] = i*section_size;
        recvcount[comm_sz-1] = size-(section_size*(comm_sz-1));
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Gatherv(tmp, section_size, MPI_INT, data, recvcount, displacement, MPI_INT, 0, MPI_COMM_WORLD);
    free(recvcount);
    free(displacement);
}

void quick_slave(int numEl){
    MPI_Barrier(MPI_COMM_WORLD);
    int displacement[comm_sz];
    int sendcount[comm_sz];
    int old_disp;

    int i, j;
    if (my_rank==0){
        for(i=0; i< numEl; i++){
            displacement[i]=i*(size/numEl);
            if(i!=numEl-1) sendcount[i]=(size/numEl);
            else sendcount[numEl-1]= size-(section_size * (numEl -1));
            if(i!=0){
                MPI_Send(&(sendcount[i]), 1, MPI_INT, i, 1, MPI_COMM_WORLD);
                MPI_Send(data+displacement[i], sendcount[i], MPI_INT, i, 1, MPI_COMM_WORLD);
            } else {
                section_size = (size/numEl);
                tmp = (int *) realloc(tmp, section_size*sizeof(int));
                for(j =0; j< section_size; j++)
                    tmp[j] = data[j];
            }
        }
    }
    else if ( my_rank != 0&& my_rank < numEl){
        MPI_Recv(&section_size, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
        tmp = (int *) realloc( tmp, sizeof(int) * (section_size) );
        MPI_Recv(tmp, section_size, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if ( my_rank <numEl){
        old_disp = size/(numEl*2);
        merge(0, old_disp-1 , section_size-1);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if ( my_rank <numEl){
        if (my_rank!=0) {
            MPI_Send(tmp, section_size, MPI_INT, 0, 2, MPI_COMM_WORLD);
        } else {
            for(i =1; i< numEl; i++)
                MPI_Recv(data+displacement[i], sendcount[i], MPI_INT, i, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            for(j = 0; j < displacement[1]; j++)
                data[j] = tmp[j];
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);
}

void quick_sort (int *temp, int n) {
    if (n < 2)
        return;
    int t;
    int p = temp[n / 2];
    int *l = temp;
    int *r = temp + n - 1;
    while (l <= r) {
        if (*l < p) {
            l++;
            continue;
        }
        if (*r > p) {
            r--;
            continue;
        }
        t = *l;
        *l++ = *r;
        *r-- = t;
    }
    quick_sort(temp, r - temp + 1);
    quick_sort(l, temp + n - l);
    return;

}

void merge (int lo, int mid, int hi) {
    int i,j,k;
    int *helper = (int *) malloc( sizeof(int) * section_size);
    for( i = lo; i<=hi; i++){
        helper[i] = tmp[i];
    }
    i = lo;
    j = mid + 1;
    k = lo;
    while (i <= mid && j <= hi) {
        if (helper[i] <= helper[j]) {
            tmp[k] = helper[i];
            i++;
        } else {
            tmp[k] = helper[j];
            j++;
        }
        k++;
    }

    while (i <= mid) {
        tmp[k] = helper[i];
        k++;
        i++;
    }
    free(helper);
    return;

}
