#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>

#define SEED 19920414

int main (int argc, char *argv[]){
    int thread_count, i, n;
    struct drand48_data rand;
    double sum, global_sum, start_one, end_one, start_two, end_two;
    double *local_sums;


    thread_count = strtol(argv[1], NULL, 10);
    n = strtol(argv[2], NULL, 10);
    local_sums = malloc( n * sizeof(double));
    start_one = omp_get_wtime();
    #pragma omp parallel private(rand) num_threads(thread_count) reduction(+:sum)
    {
        int my_rank  = omp_get_thread_num();
        double x;
        srand48_r( time(NULL)<<(my_rank*(my_rank+1)), &rand );
        for(i =0; i< n/thread_count; i++){
            drand48_r(&rand, &x);
            sum +=x;
        }
    }
    //printf("sum: %f\n", sum);
    end_one = omp_get_wtime();
    printf("\n# of Threads = %d\nNumberSize = %d\n", thread_count, n);
    printf("V1: time: %f\n", (end_one-start_one) );
    start_two = omp_get_wtime();
    #pragma omp parallel private(rand) num_threads(thread_count) reduction(+:global_sum)
    {
        int my_rank  = omp_get_thread_num();
        double x;
        srand48_r( time(NULL)<<(my_rank*(my_rank+1)), &rand );
        for(i =0; i< n/thread_count; i++){
            drand48_r(&rand, &x);
            local_sums[my_rank] += x;
        }
        for (i = 0; i < thread_count; i++)
            global_sum += local_sums[i];
    }
    end_two = omp_get_wtime();
//    printf("Global Sum: %f\n", global_sum);
    printf("V2. time: %f\n", (end_two-start_two) );
    return EXIT_SUCCESS;
}

